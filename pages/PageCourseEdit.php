<?php
require_once dirname(__FILE__).'/PageData.php';
class PageCourseEdit extends PageData {
	protected $_code;
	function __construct() {
		$this->_title_ = 'Edit Course Info';
		parent::__construct();
		if (!isset($_GET['code']))
			$this->throw_debug("Invalid course!");
		$this->_code = $_GET['code'];
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function mod_check() {
	var chk_form = document.getElementById('form_course');
	chk_form.cCode.disabled = false;
	return true;
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		if ($this->_code==null)
			$this->throw_debug('No code!');
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		$user = $this->_dodata->getProfile();
		$item = $this->_dodata->findCourse($this->_code);
		if ($item['stat']==false)
			$this->throw_debug('Something is WRONG!');
		$view = $this->_doview;
		$view->insert_page_title();
		$list = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($list);
		// create form
		$form = $view->create_form('form_course');
		$form->insert_onsubmit('javascript:return mod_check();');
		$view->create_form_input_hidden($form,'cCoId',null,$item['id']);
		$view->create_form_input_text($form,'Course Code','cCode',
			[ "tval"=>$this->_code , "ro"=>1 , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Course Name','cName',
			[ "tval"=>$item['name'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Course Unit','cUnit',
			[ "tval"=>$item['unit'] , "linebr"=>"1" ]);
		// select semester offered
		$view->create_form_label($form,'Select Semester:&nbsp;&nbsp;');
		$mark = ($item['asem']&COURSE_SEMESTER1) ? true : false;
		$view->create_form_input_checkbox($form,
			'&nbsp;Semester 1&nbsp;&nbsp;','cSem1',["chkd"=>$mark]);
		$mark = ($item['asem']&COURSE_SEMESTER2) ? true : false;
		$view->create_form_input_checkbox($form,
			'&nbsp;Semester 2&nbsp;&nbsp;','cSem2',["chkd"=>$mark]);
		$view->create_form_submit($form,'Submit','postCourseEdit');
		$view->insert_form($form,[ "class" => "w3-margin-left" ]);
		$view->insert_menu($list);
	}
}
?>
