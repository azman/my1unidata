<?php
require_once dirname(__FILE__).'/PageDone.php';
class PageCourseCh extends PageDone {
	function __construct() {
		parent::__construct('Edit Course Info');
		// check params
		if (empty($_POST['cCode'])||empty($_POST['cName'])||
				empty($_POST['cUnit'])||empty($_POST['cCoId'])) {
			throw new Exception('Invalid course info?!');
		}
		$asem = 0x00;
		if (array_key_exists('cSem1',$_POST)&&$_POST['cSem1']=="on")
			$asem |= COURSE_SEMESTER1;
		if (array_key_exists('cSem2',$_POST)&&$_POST['cSem2']=="on")
			$asem |= COURSE_SEMESTER2;
		// change course
		$this->_dodata->modifyCourse($_POST['cCoId'],$_POST['cCode'],
			$_POST['cName'],$_POST['cUnit'],$asem);
		// prepare page
		$this->_dotext = 'Course '.$_POST['cCode'].' modified.';
	}
	function build_page() {
		parent::build_page();
	}
}
?>
