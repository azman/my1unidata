<?php
require_once dirname(__FILE__).'/Page.php';
class PageInit extends Page {
	function __construct() {
		parent::__construct();
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$list = $this->_doview->menu_list_item_command(null,'login','Login');
		$this->_doview->insert_menu($list);
	}
}
?>
