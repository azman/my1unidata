<?php
require_once dirname(__FILE__).'/PageData.php';
class PageSemCourseEdit extends PageData {
	protected $_ssem;
	protected $_code;
	protected $_ccmp;
	function __construct() {
		parent::__construct('Edit Course Component');
		if (!isset($_GET['ssem'])||!isset($_GET['code'])||!isset($_GET['ccmp']))
			$this->throw_debug('Invalid course component!');
		$ssem = strtoupper(trim($_GET['ssem']));
		$code = strtoupper(trim($_GET['code']));
		$ccmp = intval($_GET['ccmp']);
		$this->_ssem = $ssem;
		$this->_code = $code;
		$this->_ccmp = $ccmp;
	}
	function build_page() {
		if ($this->_ssem==null||$this->_code==null||$this->_ccmp==null)
			$this->throw_debug('No info!');
		$view = $this->_doview;
		$view->insert_page_title();
		$user = $this->_dodata->getProfile();
		$core = $this->_dodata->findCourse($this->_code);
		if ($core['stat']==false)
			$this->throw_debug('Something is WRONG!');
		$text = $this->_dodata->selectSession($this->_ssem);
		$cors = $this->_dodata->listCoursesComponents($core['id']);
		if ($cors['stat']==false)
			$this->throw_debug('Something is WRONG!');
		$item = null;
		foreach ($cors['list'] as $temp) {
			if ($temp['id']==$this->_ccmp) {
				$item = $temp;
				//??break;
			}
		}
		if ($item==null)
			$this->throw_debug('Cannot find that component!');
		$list = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($list);
		// create form
		$form = $view->create_form('form_ccomp');
		$form->insert_onsubmit('javascript:return post_check();');
		$view->create_form_input_hidden($form,'cCCID',null,$this->_ccmp);
		$view->create_form_input_text($form,'Name','cName',
			[ "tval"=>$item['name'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Label','cText',
			[ "tval"=>$item['lbl'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Raw','cRawM',
			[ "tval"=>$item['raw'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Percentage','cPctM',
			[ "tval"=>$item['pct'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Group','cGrpM',
			[ "tval"=>$item['grp'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Sub-group','cSubM',
			[ "tval"=>$item['sub'] , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Index','cIdxM',
			[ "tval"=>$item['idx'] , "linebr"=>"1" ]);
		$view->create_form_submit($form,'Submit','postChCComp');
		$view->insert_form($form,[ "class" => "w3-margin-left" ]);
		$view->insert_menu($list);
	}
}
?>
