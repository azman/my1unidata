<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageStaffSemCh extends PageDone {
	use TraitUniData;
	function __construct() {
		if (empty($_POST['code'])||empty($_POST['ssem'])||
				empty($_POST['postSelectStaffSem'])) {
			$this->throw_debug('Invalid staff sem selection info?!');
		}
		$code = $_POST['code'];
		$ssem = intval($_POST['ssem']);
		parent::__construct('Assign Staff for '.$code.' - '.$ssem);
		// selected implementation
		$list = $this->_dodata->listCoursesStaffs(null,$code,$ssem);
		//$this->throw_debug('List:'.json_encode($list));
		$text = "<b>Staff Assignment for ".$_POST['code']."</b><br><br>\n";
		$coid = null;
		$look = [];
		$skip = true;
		// update all id as int (do i need this???)
		// - also, get coid (should i write a function for this?)
		foreach ($list['list'] as &$item) {
			$item['coid'] = intval($item['coid']);
			$item['stid'] = intval($item['stid']);
			$item['role'] = intval($item['role']);
			if ($coid===null) $coid = $item['coid'];
			if ($item['role']===COURSE_ADMIN||$item['role']===COURSE_CHECK)
				continue;
			$find = null;
			foreach ($_POST['pickStaff'] as $staf) {
				if ($staf===$item['staff']) {
					$find = $staf;
					array_push($look,$item['stid']);
				}
			}
			if ($find===null) {
				// remove if not selected!
				$coid = $item['coid'];
				$stid = $item['stid'];
				$clvl = COURSE_CHECK;
				$text = $text."Removing {".$item['staffname']."}<br>\n";
				$this->_dodata->createCoursesStaffs($coid,$stid,$ssem,$clvl);
				$skip = false;
			}
		}
		// update course assignment
		foreach ($_POST['pickStaff'] as $staf) {
			$temp = $this->_dodata->findStaff($staf);
			if ($temp['stat']!==true)
				$this->throw_debug('Invalid staff unid!'.json_encode($temp));
			$stid = $temp['id'];
			if (in_array($stid,$look)) continue;
			$clvl = COURSE_STAFF;
			$text = $text."Adding {".$temp['name']."}<br>\n";
			$this->_dodata->createCoursesStaffs($coid,$temp['id'],$ssem,$clvl);
			$skip = false;
		}
		if ($skip===true) $text = $text."Nothing to update!";
		else $text = $text."<br>Staff assignment updated!";
		$this->_dotext = $text;
	}
}
?>
