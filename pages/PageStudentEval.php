<?php
require_once dirname(__FILE__).'/PageData.php';
class PageStudentEval extends PageData {
	protected $_code;
	protected $_ssem;
	function __construct() {
		if (empty($_GET['code'])||empty($_GET['ssem'])) {
			$this->throw_debug('Not enough info!');
		}
		$this->_code = $_GET['code'];
		$this->_ssem = intval($_GET['ssem']);
		parent::__construct();
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function mod_check() {
	var pckL = document.getElementsByName('pickStaff');
	if (pckL[0].value==="") {
		alert('Lecture not selected!');
		return false;
	}
	var pckI = document.getElementsByName('pickLabIn');
	if (pckI[0].value==="") {
		alert('Instructor not selected!');
		return false;
	}
	var chk_form = document.getElementById('form_studeval');
	// make sure ALL radiogroup is selected!
	for (var loop=1;loop<=9;loop++) {
		var name = "A"+loop;
		var rads = document.getElementsByName(name);
		var done = false;
		for (var next=0;next<rads.length;next++) {
			if (rads[next].checked) {
				done = true;
				break;
			}
		}
		if (!done) {
			alert('Question '+name+' not answered!');
			return false;
		}
	}
	for (var loop=1;loop<=7;loop++) {
		var name = "B"+loop;
		var rads = document.getElementsByName(name);
		var done = false;
		for (var next=0;next<rads.length;next++) {
			if (rads[next].checked) {
				done = true;
				break;
			}
		}
		if (!done) {
			alert('Question '+name+' not answered!');
			return false;
		}
	}
	for (var loop=1;loop<=8;loop++) {
		var name = "C"+loop;
		var rads = document.getElementsByName(name);
		var done = false;
		for (var next=0;next<rads.length;next++) {
			if (rads[next].checked) {
				done = true;
				break;
			}
		}
		if (!done) {
			alert('Question '+name+' not answered!');
			return false;
		}
	}
	return true;
}
function pickLec(selectObject) {
	var value = selectObject.value;
	for (loop=0;loop<selectObject.length;loop++) {
		if (selectObject[loop].value==value) {
			//console.log("Lecturer:"+selectObject[loop].label);
			var elem = document.getElementById('thatLEC');
			elem.innerHTML = selectObject[loop].label;
		}
	}
}
function pickIns(selectObject) {
	var value = selectObject.value;
	for (loop=0;loop<selectObject.length;loop++) {
		if (selectObject[loop].value==value) {
			//console.log("Instructor:"+selectObject[loop].label);
			var elem = document.getElementById('thatINS');
			elem.innerHTML = selectObject[loop].label;
		}
	}
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		$code = $this->_code;
		$ssem = $this->_ssem;
		$user = $this->_dodata->getProfile();
		$cors = $this->_dodata->findCourse($code);
		if ($cors['stat']!==true)
			$this->throw_debug('Invalid course code!');
		$coid = $cors['id'];
		$temp = $this->_dodata->selectSession($ssem);
		$menu = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($menu);
		$view->insert_page_section("Student Evaluation for ".$code." ".$temp);
		$form = $view->create_form('form_studeval');
		$form->insert_onsubmit('javascript:return mod_check();');
		$view->insert_form($form,[ "fwpct" => 80 ]);
		$view->create_form_input_hidden($form,'code',null,$code);
		$view->create_form_input_hidden($form,'unid',null,$user['unid']);
		$view->create_form_input_hidden($form,'stid',null,$user['id']);
		$view->create_form_input_hidden($form,'ssem',null,$ssem);
		$view->create_form_input_hidden($form,'coid',null,$coid);
		include_once dirname(__FILE__).'/../include/config-survey.php';
		// get list of staff involved
		$opts_staf = [];
		$staf = $this->_dodata->listCoursesStaffs(null,$code,$ssem);
		$pick = null;
		foreach ($staf['list'] as $item) {
			$item['role'] = intval($item['role']);
			if ($item['role']===COURSE_CHECK) continue;
			if ($pick===null&&$item['role']===COURSE_ADMIN) {
				$pick = true;
			}
			array_push($opts_staf,[$item['staffname'],$item['stid'],$pick]);
			if ($pick===true) $pick = false;
		}
		if (count($opts_staf)===0)
			$this->throw_debug('No staff available???');
		// selection for lecturer
		$opid = "pickStaff";
		$temp = $view->create_form_select($form,
			"Select Lecturer (who handles your lecture sessions)",
			$opid,[],["lbrk"=>true,"linebr"=>2]);
		//$temp->insert_id($opid);
		$otmp = $view->create_form_select_option($form,$temp,
			"Select Your Lecturer","",true);
		$otmp->insert_constant("disabled");
		foreach ($opts_staf as $that) {
			$otmp = $view->create_form_select_option($form,$temp,
				$that[0],$that[1],false);
		}
		$temp->insert_keyvalue("onchange","pickLec(this)");
		// selection for lab instructor
		$opid = "pickLabIn";
		$temp = $view->create_form_select($form,
			"Select Lab Instructor (who handles your lab sessions)",
			$opid,[],["lbrk"=>true,"linebr"=>2]);
		//$temp->insert_id($opid);
		$otmp = $view->create_form_select_option($form,$temp,
			"Select Your Lab Instructor","",true);
		$otmp->insert_constant("disabled");
		foreach ($opts_staf as $that) {
			$otmp = $view->create_form_select_option($form,$temp,
				$that[0],$that[1],false);
		}
		// convert opts_rateX to my format
		$temp->insert_keyvalue("onchange","pickIns(this)");
		foreach ($opts_rateX as &$item) {
			$swap = $item[0];
			$item[0] = "&nbsp;".$item[1]."&nbsp;&nbsp;";
			$item[1] = $swap;
		}
		// the survey questions
		$view->create_form_label($form,$text_partA,["linebr"=>2]);
		foreach ($opts_partA as $key => $val) {
			$view->create_form_input_radio_group($form,$key.". ".$val,
				$key,$opts_rateX,["lbrk"=>true,"linebr"=>2]);
		}
		$view->create_form_label($form,$text_partB." - <i>Lecturer</i>:");
		$temp = $view->create_form_label($form,
			"(Please use drop-down box above to select)",["linebr"=>2]);
		$temp->insert_id("thatLEC");
		foreach ($opts_partB as $key => $val) {
			$view->create_form_input_radio_group($form,$key.". ".$val,
				$key,$opts_rateX,["lbrk"=>true,"linebr"=>2]);
		}
		$view->create_form_label($form,$text_partC." - <i>Instructor</i>:");
		$temp = $view->create_form_label($form,
			"Please use drop-down box above to select",["linebr"=>2]);
		$temp->insert_id("thatINS");
		foreach ($opts_partC as $key => $val) {
			$view->create_form_input_radio_group($form,$key.". ".$val,
				$key,$opts_rateX,["lbrk"=>true,"linebr"=>2]);
		}
		$text_partD = "<b>PART D: Please provide any comments for ".
			"improvement.</b>";
		$temp = $view->create_form_label($form,$text_partD);
		$temp = $view->create_form_textarea($form,"Comments",'comment',
			[ "lbrk"=>true,"linebr"=>1]);
		//$temp->insert_keyvalue('form','form_studeval');
		$temp->insert_keyvalue('rows',10);
		// submit button only if student
		if ($user['staf']===false)
			$view->create_form_submit($form,'Submit','postChStudentEval');
		// create back link
		$view->insert_menu($menu);
	}
}
?>
