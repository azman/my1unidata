<?php
require_once dirname(__FILE__).'/PageDone.php';
class PageSemCourseCh extends PageDone {
	function __construct() {
		if (empty($_POST['cCCID'])||empty($_POST['cName'])||
				empty($_POST['cText'])||empty($_POST['cRawM'])||
				empty($_POST['cPctM'])||empty($_POST['cGrpM'])||
				empty($_POST['cSubM'])||empty($_POST['cIdxM'])) {
			$this->throw_debug('Invalid course component info?!');
		}
		parent::__construct();
		// update
		$this->_dodata->modifyCoursesComponents($_POST['cCCID'],
			$_POST['cName'],$_POST['cText'],
			$_POST['cRawM'],$_POST['cPctM'],
			$_POST['cGrpM'],$_POST['cSubM'],$_POST['cIdxM']);
		$this->done_header("Edit Course component");
		$this->done_message("Course component info updated!");
	}
}
?>
