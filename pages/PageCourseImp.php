<?php
require_once dirname(__FILE__).'/PageData.php';
class PageCourseImp extends PageData {
	function __construct() {
		parent::__construct('Implementation List');
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		// create return link
		$list = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($list);
		// get to it!
		$user = $this->_dodata->getProfile();
		$imps = $this->_dodata->listCourseImps();
		if ($imps['stat']===true&&count($imps['list'])>0) {
			$ssem = null; $size = 0;
			foreach ($imps['list'] as $item) {
				// check if new session
				if ($item['ssem']!==$ssem) {
					$size++;
					if ($user['type']!==USER_ADMIN&&$size>2) break;
					// remember this session
					$ssem = $item['ssem'];
					$text = $this->_dodata->getSessionLabel($ssem);
					// create table
					$ttab = $view->create_table();
					$view->insert_table($ttab);
					// create label
					$view->create_table_header_row($ttab);
					$tcol = $view->create_table_header_col($ttab);
					$tcol->insert_keyvalue('colspan',3,true);
					$tcol->insert_class("w3-center");
					$tcol->insert_inner($text);
					$view->create_table_header_row($ttab);
					$tcol = $view->create_table_header_col($ttab);
					$tcol->insert_inner('Course Code');
					$tcol = $view->create_table_header_col($ttab);
					$tcol->insert_inner('Course Name');
					$tcol = $view->create_table_header_col($ttab);
					$tcol->insert_inner('Implementation');
				}
				$part = explode('_',$item['name']);
				//$part[0] ==> $code
				//$part[1] ==> $ssem
				$that = $this->_dodata->findCourse($part[0]);
				if ($that['stat']===false)
					$that['name'] = "Unknown";
				$view->create_table_data_row($ttab);
				$tcol = $view->create_table_data_col($ttab);
				$temp = $part[0];
				$tcol->insert_inner($temp);
				$tcol = $view->create_table_data_col($ttab);
				$temp = $that['name'];
				$tcol->insert_inner($temp);
				$tcol = $view->create_table_data_col($ttab);
				$temp = $item['name'];
				$code = $part[0];
				$ssem = $part[1];
				$step = 0;
				if ($user['type']===USER_ADMIN) {
					$step++;
					$show = $view->create_menu_item("removeimp&cimp=".
						$item['name'],"Remove",MENUITEM_CMD_,
						[ "color" => "w3-red" ]);
					$tcol->append_object($show);
					$show = $view->create_menu_item("impcourse&ssem=".
						$ssem."&code=".$code,"View",MENUITEM_CMD_,
						[ "color" => "w3-green" ]);
					$tcol->append_object($show);
				}
				if ($this->_dodata->testEvalReady($code,$ssem)===EVAL_DONE) {
					// link to view student evaluation
					$step++;
					$show = $view->create_menu_item("vieweval&code=".
						$code."&ssem=".$ssem,"View Student Evaluation",
						MENUITEM_CMD_);
					$tcol->append_object($show);
					if ($user['type']===USER_ADMIN) {
						$table = $code.'_'.$ssem;
						$tstat = $table.'_EVALSTAT';
						$list1 = $this->_dodata->listCourseStudent($table);
						$list2 = $this->_dodata->listCourseEvalStat($tstat);
						if (isset($list1['list'])&&isset($list2['list'])) {
							$size1 = count($list1['list']);
							$size2 = count($list2['list']);
							if ($size2<$size1) {
								$show = $view->create_menu_item("makeeval&".
									"code=".$code."&ssem=".$ssem,
									"Update Students' Status for Evaluation",
									MENUITEM_CMD_);
								$tcol->append_object($show);
							}
						}
					}
				}
				if ($step>0) $temp = $temp."&nbsp;&nbsp;";
				$tcol->insert_inner($temp);
			}
		}
		else $view->insert_highlight('No implementation found in database.');
		$view->insert_menu($list);
	}
	function sendCSV($template=false) {
		$head =  [ HEADER_COURSE_CODE,
			HEADER_COURSE_NAME, HEADER_COURSE_UNIT, HEADER_COURSE_ASEM ];
		$data = [];
		if ($template===false) {
			$cors = $this->_dodata->listCourses();
			foreach ($cors['list'] as $item) {
				array_push($data,[$item['code'],
					$item['name'],$item['unit'],$item['asem']]);
			}
		}
		require_once dirname(__FILE__).'/FileText.php';
		$fcsv = new FileText();
		$fcsv->sendCSV('listcourse.csv',$head,$data);
	}
}
?>
