<?php
require_once dirname(__FILE__).'/../include/TraitChart.php';
require_once dirname(__FILE__).'/PageData.php';
define('TEMPLATE_STUDENT',0);
define('TEMPLATE_COURSEC',1);
define('TEMPLATE_STMARKS',2);
class PageSemCourse extends PageData {
	use TraitChart;
	protected $_ssem;
	protected $_code;
	protected $_temp;
	function __construct() {
		if (!isset($_GET['ssem'])||!isset($_GET['code'])) {
			$this->_temp = true;
			$full = "Template";
			$this->_ssem = null;
			$this->_code = null;
		}
		else {
			$this->_temp = false;
			$ssem = strtoupper(trim($_GET['ssem']));
			$code = strtoupper(trim($_GET['code']));
			$full = $code."@".$ssem;
			$this->_ssem = $ssem;
			$this->_code = $code;
		}
		parent::__construct($full);
		$temp = $this->cssobj_chart();
		$this->_head->append_object($temp);
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$code = $this->_code;
		$ssem = $this->_ssem;
		$user = $this->_dodata->getProfile();
		$core = $this->_dodata->findCourse($code);
		$temp = $this->_dodata->selectSession($ssem);
		$cors = $this->_dodata->listCoursesComponents($core['id']);
		if ($cors['stat']==false)
			$this->throw_debug('Cannot find components!');
		$view->insert_highlight($code." ".$core['name']." (".$temp.")");
		$keep = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$menu = $keep;
		// list staff for this implementation
		$staf = $this->_dodata->listCoursesStaffs(null,$code,$ssem);
		// find user's role
		$what = null;
		foreach ($staf['list'] as &$that) {
			$that['coid'] = intval($that['coid']);
			$that['stid'] = intval($that['stid']);
			$that['role'] = intval($that['role']);
			if ($what===null&&$that['stid']===$user['id'])
				$what = $that;
		}
		if ($what===null&&$user['type']!==USER_ADMIN)
			$this->throw_debug('User not assigned here!');
		$list = [];
		foreach ($staf['list'] as &$that) {
			$temp = "";
			if ($that['stid']===$user['id']) $temp = "#";
			if ($that['role']===COURSE_ADMIN) {
				$text = "(Coordinator) $temp".$that['staffname'];
				$item = $view->create_panel($text,
					[ "class" => "w3-panel w3-light-grey" ]);
				array_push($list,$item);
			}
			else if ($that['role']===COURSE_STAFF) {
				$text = "(Staff) $temp".$that['staffname'];
				$item = $view->create_panel($text,
					[ "class" => "w3-panel w3-light-grey" ]);
				array_push($list,$item);
				if ($what!==null&&$what['role']===COURSE_ADMIN) {
					$coid = $that['coid'];
					$stid = $that['stid'];
					$temp = $view->create_menu_item("removesemstaff&coid=".
						$coid."&stid=".$stid."&ssem=".$ssem,
						"Remove",MENUITEM_CMD_,[ "color" => "w3-red" ]);
					$item->insert_object($temp);
				}
			}
		}
		if ($what!==null&&$what['role']===COURSE_ADMIN) {
			$menu = $view->menu_list_item_command($menu,
				"selectsemstaff&&code=$code&ssem=$ssem","Manage Staff");
		}
		// link to view student evaluation form
		$menu = $view->menu_list_item_command($menu,
			"studeval&code=$code&ssem=$ssem","View Student Evaluation Form");
		if ($this->_dodata->testEvalReady($code,$ssem)===EVAL_DONE) {
			// link to view student evaluation
			$menu = $view->menu_list_item_command($menu,
				"vieweval&code=$code&ssem=$ssem",
				"View Student Evaluation Status");
			if ($what!==null&&$what['role']===COURSE_ADMIN) {
				$table = $this->_code.'_'.$this->_ssem;
				$list1 = $this->_dodata->listCourseStudent($table);
				$size1 = count($list1['list']);
				$tstat = $table.'_EVALSTAT';
				$list2 = $this->_dodata->listCourseEvalStat($tstat);
				$size2 = count($list2['list']);
				if ($size2<$size1) {
					// link to update student evaluation status
					$menu = $view->menu_list_item_command($menu,
						"makeeval&code=$code&ssem=$ssem",
						"Update Students' Status for Evaluation");
				}
			}
		}
		else if ($what!==null&&($what['role']===COURSE_ADMIN||
				$what['role']===COURSE_STAFF)) {
			// link to allow student evaluation
			$menu = $view->menu_list_item_command($menu,
				"makeeval&code=$code&ssem=$ssem",
				"Enable Student Evaluation");
		}
		$view->insert_menu($menu);
		// show staff
		$view->insert_page_section("Staff for $code");
		foreach ($list as $item)
			$view->insert_object($item);
		// checkif student db available
		$this->_dodata->checkStudents();
		// list students as well
		$table = $this->_code.'_'.$this->_ssem;
		$this->_dodata->checkCourseStudent($table,$core['id']);
		$list = $this->_dodata->listCourseStudent($table);
		if ($list['stat']===true) {
			// chart stuff: labels - 12 grades
			$chart_lbls = array('A','A-','B+','B','B-','C+','C',
				'C-','D+','D','D-','F');
			$chart_bars = count($chart_lbls);
			$chart_vals = array();
			for ($loop=0;$loop<$chart_bars;$loop++) {
				$chart_vals[$chart_lbls[$loop]] = 0;
			}
			$chart_make = false;
			$view->insert_page_section("Student Marks");
			$menu = $keep;
			$menu = $view->menu_list_item_command($menu,
				"stmarkcsv&ssem=".$this->_ssem."&code=".$this->_code,
				"Download Student Marks (CSV)");
			$view->insert_menu($menu);
			// create form
			$form = $view->create_form('form_upmark');
			$form->insert_keyvalue('enctype','multipart/form-data');
			$view->create_form_input_hidden($form,'unid',null,$user['unid']);
			$view->create_form_input_hidden($form,'codeId',null,$this->_code);
			$view->create_form_input_hidden($form,'ssemId',null,$this->_ssem);
			$view->create_form_input_hidden($form,'aCmd',
				null,TASK_STAFF_UPDATE_MARKS);
			$view->create_form_input_file($form,'Data File (CSV)','dataFile',
				["linebr"=>1]);
			$view->create_form_submit($form,'Upload Marks','postUploadMarks');
			$view->insert_form($form,["class"=>"w3-margin-top"]);
			// create table
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			for ($loop=0;$loop<2;$loop++) {
				$view->create_table_header_row($ttab);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('#');
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_NAME);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_UNID);
				if (!$loop) $wide = 3;
				foreach ($cors['list'] as $item) {
					$tcol = $view->create_table_header_col($ttab);
					$tcol->insert_inner($item['name']);
					if (!$loop) $wide++;
				}
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('Total');
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('Full');
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('%');
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('?');
				if (!$loop) $wide += 4; 
				$step = 0; $skip = 0;
				foreach ($list['list'] as $item) {
					if ($loop==0) {
						if (!DataStudent::is_active($item)) {
							$skip++; // count inactives
							continue;
						}
					}
					else if (DataStudent::is_active($item)) continue;
					$step++; // count active students only
					$view->create_table_data_row($ttab);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($step);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['name']);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['unid']);
					$init = 0.0; $full = 0.0;
					foreach ($cors['list'] as $mark) {
						$temp = strtolower($mark['name']);
						$tcol = $view->create_table_data_col($ttab);
						if ($item[$temp]!==null) {
							$temp = floatval($item[$temp]);
							$show = number_format($temp,2,'.','');
							if ($temp>$mark['raw']) $show = $show."**";
							else {
								$init += $temp*$mark['pct']/$mark['raw'];
								$full += $mark['pct'];
							}
							$tcol->insert_inner($show);
						}
					}
					if ($full>0) {
						$tpct = (float)$init*100/$full;
						$chart_make = true;
					}
					else $tpct = 0.0;
					// total
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner(number_format((float)$init,2,'.',''));
					// full mark
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner(number_format((float)$full,2,'.',''));
					// percentage
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner(number_format((float)$tpct,2,'.',''));
					// grade
					$gred = ($full==0.0)?'-':DataCourse::get_grade($tpct);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($gred);
					if (!$loop&&$gred!=='X'&&$gred!=='-')
						$chart_vals[$gred]++;
				}
				if (!$loop) {
					if (!$skip) break;
					// create table for inactive list
					$ttab = $view->create_table();
					$view->insert_table($ttab);
					$view->create_table_header_row($ttab);
					$tcol = $view->create_table_header_col($ttab,
							[ "class"=>"w3-center" ]);
					$tcol->insert_keyvalue('colspan',$wide,true);
					$tcol->insert_inner("Inactive Student(s)");
				}
			}
			if ($chart_make===true) {
				// chart for current carry marks
				$view->insert_page_section("Current Marks Distribution");
				// show off svg stuffs!
				$svgd = new SVG(null,640,480);
				$svgg = $svgd->create_g($svgd,null);
				$svgg->transform("translate(40,40)");
				$svgx = $svgd->create_g($svgg,"x axis");
				$svgx->transform("translate(0,380)");
				// loop through AND find max
				$chart_dmax = 0;
				$xlen = 35; $xoff = 5 + $xlen; $xnxt=$xoff;
				for ($loop=0;$loop<$chart_bars;$loop++,$xnxt+=$xoff) {
					$svgt = $svgd->create_g($svgx,"x tick");
					$svgt->transform("translate(".$xnxt.",0)");
					$svgt->draw_line(null,"0","6");
					$gred = $chart_lbls[$loop];
					$svgt->draw_text($gred,null,"0","9",null,".71em");
					if ($chart_vals[$gred]>$chart_dmax)
						$chart_dmax = $chart_vals[$gred];
				}
				// round up to nearest 10s
				$chart_dnxt = $chart_dmax % 10;
				if ($chart_dnxt)
					$chart_dnxt = (10-$chart_dnxt)+$chart_dmax;
				else $chart_dnxt = $chart_dmax;
				$svgx->draw_path("domain","M0,0H560");
				$svgy = $svgd->create_g($svgg,"y axis");
				$svgy->transform("translate(0,-20)");
				// draws half and max ticks - 400 pixels high?
				$ylen = 40; $yoff = 0;
				// 100% tick
				$svgt = $svgd->create_g($svgy,"y tick");
				$svgt->transform("translate(0,".$yoff.")");
				$svgt->draw_line(null,"-6","0");
				$svgt->draw_text("".$chart_dnxt,null,"-9","0",null,".32em");
				// 50% tick
				$yoff += $ylen * 5;
				$svgt = $svgd->create_g($svgy,"y tick");
				$svgt->transform("translate(0,".$yoff.")");
				$svgt->draw_line(null,"-6","0");
				$svgt->draw_text("".($chart_dnxt/2),null,"-9","0",null,".32em");
				$svgy->draw_path("domain","M0,0V400");
				// draw bars
				$xnxt=$xoff-15;
				for ($loop=0;$loop<$chart_bars;$loop++,$xnxt+=$xoff) {
					$gred = $chart_lbls[$loop];
					$gdat = $chart_vals[$gred];
					$yval = 400*$gdat/$chart_dnxt;
					$yoff = -20 + 400 - $yval;
					$svgb = $svgd->create_g($svgg,"gdat");
					$svgb->draw_rectangle("bar",$xnxt,$yoff,$xlen,$yval);
					$svgb->draw_text($gdat,null,
						($xnxt+5),($yoff-5),null,".16em");
				}
				$view->insert_block($svgd);
			}
		}
		// student list
		$view->insert_page_section("Student List");
		$menu = $keep;
		$menu = $view->menu_list_item_command($menu,
			"cstudcsv&ssem=".$this->_ssem."&code=".$this->_code,
			"Download Student List (CSV)");
		$view->insert_menu($menu);
		// create form
		$form = $view->create_form('form_upstud');
		$form->insert_keyvalue('enctype','multipart/form-data');
		$view->create_form_input_hidden($form,'unid',null,$user['unid']);
		$view->create_form_input_hidden($form,'pickCourse',null,$this->_code);
		$view->create_form_input_hidden($form,'pickSem',null,$this->_ssem);
		$view->create_form_input_hidden($form,'aCmd',
			null,TASK_STAFF_ADD_STUDENTS);
		$view->create_form_input_file($form,'Data File (CSV)','dataFile',
			["linebr"=>1]);
		$view->create_form_submit($form,
			'Upload Student Info','postUploadStudent');
		$view->insert_form($form,["class"=>"w3-margin-top"]);
		if ($list['stat']===true) {
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			for ($loop=0;$loop<2;$loop++) {
				$view->create_table_header_row($ttab);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner('#');
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_NAME);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_UNID);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_NRIC);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_PROG);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_LGRP);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_MGRP);
				$step = 0; $skip = 0;
				foreach ($list['list'] as $item) {
					if (!$loop) {
						if (!DataStudent::is_active($item)) {
							$skip++; // count inactives
							continue;
						}
					}
					else if (DataStudent::is_active($item))  continue;
					$step++;
					$view->create_table_data_row($ttab);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($step);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['name']);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['unid']);
					if ($what!==null&&($what['role']===COURSE_ADMIN)) {
						if (DataStudent::is_active($item)) {
							// link to deactivate student
							$show = $view->create_menu_item("deactiv8&stid=".
								$item['stid']."&from=".$table,
								"Deactivate",MENUITEM_CMD_,
								[ "color" => "w3-red w3-small" ]);
						}
						else {
							// link to reactivate student
							$show = $view->create_menu_item("reactiv8&stid=".
								$item['stid']."&from=".$table,
								"Reactivate",MENUITEM_CMD_,
								[ "color" => "w3-blue w3-small" ]);
						}
						$tcol->append_object($show);
					}
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['bkid']);
					$tcol = $view->create_table_data_col($ttab);
					$pchk = $this->_dodata->findProg(null,$item['prog']);
					$tcol->insert_inner($pchk['prog']);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['lgrp']);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['mgrp']);
				}
				if (!$loop) {
					if (!$skip) break;
					// create table for inactive list
					$ttab = $view->create_table();
					$view->insert_table($ttab);
					$view->create_table_header_row($ttab);
					$tcol = $view->create_table_header_col($ttab,
							[ "class"=>"w3-center" ]);
					$tcol->insert_keyvalue('colspan',7,true);
					$tcol->insert_inner("Inactive Student(s)");
				}
			}
		}
		else {
			$text = '<i><b>No student info found in database.</b></i>';
			$view->insert_highlight($text);
		}
		if (count($cors['list'])>0) {
			$view->insert_page_section("Course Evaluation Components");
			$menu = $keep;
			$menu = $view->menu_list_item_command($menu,
				"ccompcsv&ssem=".$this->_ssem."&code=".$this->_code,
				"Download Course Components (CSV)");
			$view->insert_menu($menu);
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			$view->create_table_header_row($ttab);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_LABEL);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_NAME);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_RAW_);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_PCT_);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_GROUP);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_SUBGRP);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_CCOMP_INDEX);
			$full = 0.0;
			foreach ($cors['list'] as $item) {
				$view->create_table_data_row($ttab);
				$tcol = $view->create_table_data_col($ttab);
				$text = $item['lbl'];
				if ($what!==null&&($what['role']===COURSE_ADMIN||
						$what['role']===COURSE_STAFF)) {
					$show = $view->create_menu_item("editccmp&ssem=".
						$this->_ssem."&code=".$this->_code.
						"&ccmp=".$item['id'],"Modify",MENUITEM_CMD_);
					$tcol->insert_object($show);
					$text = $text."&nbsp;&nbsp;";
				}
				$tcol->insert_inner($text);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['name']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['raw']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['pct']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['grp']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['sub']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['idx']);
				$full += floatval($item['pct']);
			}
			if ($full!==100.0) {
				$text = number_format((float)$full,2,'.','');
				$link = "<i>Invalid total percentage ($text)!</i>";
				$text = "<span style=\"color:red\">$link</span>";
				$view->insert_highlight($text);
			}
		}
		else {
			$link = '<i>No course info found in database.</i>';
			$view->insert_highlight($link);
		}
		$menu = $keep;
		$view->insert_menu($menu);
	}
	function sendCSV($which=TEMPLATE_STUDENT) {
		switch ($which) {
			default:
			case TEMPLATE_STUDENT:
				if ($this->_temp===true)
					$this->throw_debug('Something is WRONG!');
				$head = [ HEADER_STUDENT_UNID,HEADER_STUDENT_NAME,
					HEADER_STUDENT_NRIC,HEADER_STUDENT_PROG,
					HEADER_STUDENT_LGRP,HEADER_STUDENT_MGRP ];
				$data = []; $name = "studentlist.csv";
				$this->_dodata->selectSession($this->_ssem);
				$core = $this->_dodata->findCourse($this->_code);
				$table = $this->_code.'_'.$this->_ssem;
				$this->_dodata->checkCourseStudent($table,$core['id']);
				$list = $this->_dodata->listCourseStudent($table);
				if ($list['stat']==true) {
					array_unshift($head,'#');
					$step = 0;
					foreach ($list['list'] as $item) {
						$step++;
						$pchk = $this->_dodata->findProg(null,$item['prog']);
						$prog = $pchk['prog'];
						array_push($data,[$step,$item['unid'],$item['name'],
							$item['bkid'],$prog,$item['lgrp'],$item['mgrp']]);
					}
					$name = $this->_code."_".$this->_ssem."_".$name;
				}
				else $name = "template_".$name;
				break;
			case TEMPLATE_COURSEC:
				$head = [ HEADER_CCOMP_NAME, HEADER_CCOMP_RAW_,
					HEADER_CCOMP_PCT_,HEADER_CCOMP_LABEL,
					HEADER_CCOMP_GROUP,HEADER_CCOMP_SUBGRP,
					HEADER_CCOMP_INDEX ];
				$data = []; $name = "coursecomponent.csv";
				if ($this->_temp===true) {
					$name = "template_".$name;
					// example assignment
					array_push($data,["AS1",50,5,"Assignment 1",1,1,1]);
					array_push($data,["AS2",50,5,"Assignment 2",1,1,2]);
					array_push($data,["LA1",10,10,"Lab Assessment 1",1,2,1]);
					array_push($data,["LA2",10,10,"Lab Assessment 2",1,2,2]);
					array_push($data,["LA3",10,10,"Lab Assessment 3",1,2,3]);
					array_push($data,["MTE",50,10,"Mid-Term Exam",2,1,1]);
					array_push($data,["FXM",100,50,"Final Exam",2,2,1]);
				}
				else {
					$core = $this->_dodata->findCourse($this->_code);
					$text = $this->_dodata->selectSession($this->_ssem);
					$cors = $this->_dodata->listCoursesComponents($core['id']);
					if ($cors['stat']==false)
						$this->throw_debug('Something is WRONG!');
					foreach ($cors['list'] as $item) {
						array_push($data,[$item['name'],$item['raw'],
							$item['pct'],$item['lbl'],
							$item['grp'],$item['sub'],$item['idx']]);
					}
					$name = $this->_code."_".$this->_ssem."_".$name;
				}
				break;
			case TEMPLATE_STMARKS:
				if ($this->_temp===true)
					$this->throw_debug('Something is WRONG!');
				$head = [ HEADER_STUDENT_NAME,HEADER_STUDENT_UNID ];
				$data = []; $name = "studentmarks.csv";
				// select session & course
				$core = $this->_dodata->findCourse($this->_code);
				$text = $this->_dodata->selectSession($this->_ssem);
				$cors = $this->_dodata->listCoursesComponents($core['id']);
				$table = $this->_code.'_'.$this->_ssem;
				// checkif student db available
				$this->_dodata->checkStudents();
				$this->_dodata->checkCourseStudent($table,$core['id']);
				$list = $this->_dodata->listCourseStudent($table);
				if ($cors['stat']==false)
					$this->throw_debug('Cannot find components!');
				foreach ($cors['list'] as $item)
					array_push($head,$item['name']);
				array_push($head,'Total');
				array_push($head,'Full');
				array_push($head,'%');
				$step = 0;
				foreach ($list['list'] as $item) {
					$step++; $data_temp = [];
					array_push($data_temp,$item['name']);
					array_push($data_temp,$item['unid']);
					$init = 0.0; $full = 0.0;
					foreach ($cors['list'] as $mark) {
						$temp = strtolower($mark['name']);
						if ($item[$temp]!==null)
							$temp = floatval($item[$temp]);
						else $temp = 0.0;
						$init += $temp*$mark['pct']/$mark['raw'];
						$full += $mark['pct'];
						$temp = number_format((float)$temp,2,'.','');
						array_push($data_temp,$temp);
					}
					// total
					array_push($data_temp,number_format((float)$init,2,'.',''));
					// full mark
					array_push($data_temp,number_format((float)$full,2,'.',''));
					// percentage
					if ($full>0)
						$temp = number_format((float)$init*100/$full,2,'.','');
					else $temp = "0.00";
					array_push($data_temp,$temp);
					// push into data line
					array_push($data,$data_temp);
				}
				$name = $this->_code."_".$this->_ssem."_".$name;
				break;
		}
		require_once dirname(__FILE__).'/../include/FileText.php';
		$fcsv = new FileText();
		$fcsv->sendCSV($name,$head,$data);
	}
}
?>
