<?php
require_once dirname(__FILE__).'/PageData.php';
class PageCourse extends PageData {
	function __construct() {
		$this->_title_ = 'Course List';
		parent::__construct();
	}
	function build_page() {
		$user = $this->_dodata->getProfile();
		$cors = $this->_dodata->listCourses();
		$view = $this->_doview;
		$view->insert_page_title();
		// create return link & csv download link
		$list = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$list = $view->menu_list_item_command($list,
			"viewcourse&fmt=csv","Download CSV");
		$view->insert_menu($list);
		if ($user['type']===USER_ADMIN) {
			// create form to upload more course info
			$form = $view->create_form('form_upcors');
			$form->insert_keyvalue('enctype','multipart/form-data');
			$view->create_form_input_hidden($form,'unid',null,$user['unid']);
			$view->create_form_input_hidden($form,'aCmd',
				null,TASK_STAFF_CREATE_COURSE);
			$view->create_form_input_file($form,'Data File (CSV)','dataFile',
				["linebr"=>1]);
			$view->create_form_submit($form,
				'Upload Course Info','postUploadCourseInfo');
			$view->insert_form($form,[ "class" =>
				"w3-margin-left w3-margin-top" ]);
		}
		// rearrange by semester?
		if ($cors['stat']==true) $size = count($cors['list']);
		else $size = 0;
		if ($size>0) {
			// get stats
			$cchk = 0; $chk1 = 0; $chk2 = 0;
			foreach ($cors['list'] as $item) {
				if ($item['asem']&COURSE_SEMESTER1) $chk1++;
				if ($item['asem']&COURSE_SEMESTER2) $chk2++;
				if (!$item['asem']) $cchk++;
			}
			// show if too long
			if ($size>10) {
				$info = "<i>Total Course</i>: <b>".$size."</b>";
				$info = $info."&nbsp;[<i>Semester1</i>:".$chk1."]";
				$info = $info."&nbsp;[<i>Semester2</i>:".$chk2."]";
				$view->insert_highlight($info);
			}
			$reps = $cchk>0 ? 3 : 2;
			$pick = [ "Semester 1", "Semester 2", "Unassigned" ];
			$flag = [ COURSE_SEMESTER1, COURSE_SEMESTER2, 0 ];
			$tcnt = [ $chk1 , $chk2 , $cchk ];
			for ($loop=0;$loop<$reps;$loop++) {
				// create table
				$ttab = $view->create_table();
				$view->insert_table($ttab);
				// table section
				$view->create_table_header_row($ttab,
					[ "color"=>"w3-theme-d1" ]);
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner("$pick[$loop] ($tcnt[$loop])");
				$tcol->insert_keyvalue("colspan","6");
				// table header
				$view->create_table_header_row($ttab);
				$tcol = $view->create_table_header_col($ttab,
						[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Code");
				$tcol = $view->create_table_header_col($ttab,
						[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Name");
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Unit/Credit");
				$tcol = $view->create_table_header_col($ttab,
						[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Semester");
				$tcol = $view->create_table_header_col($ttab,
						[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Coordinator");
				$tcol = $view->create_table_header_col($ttab,
						[ "class"=>"w3-center" ]);
				$tcol->insert_inner("Staff");
				// table data
				foreach ($cors['list'] as $item) {
					if (!($item['asem']&$flag[$loop]))
						continue;
					$view->create_table_data_row($ttab);
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($item['code']."&nbsp;&nbsp;");
					$show = $view->create_menu_item("editcourse&code=".
						$item['code'],"Modify",MENUITEM_CMD_);
					$tcol->append_object($show);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['name']);
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($item['unit']);
					// check semester
					$text = "";
					if ($item['asem']&COURSE_SEMESTER1) {
						if ($text!="") $text = $text.",";
						$text = $text."SEM1";
					}
					if ($item['asem']&COURSE_SEMESTER2) {
						if ($text!="") $text = $text.",";
						$text = $text."SEM2";
					}
					if ($text=="") $text = "Not assigned!";
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($text);
					// course staffs
					$this->_dodata->checkCourseStaff();
					$staf = $this->_dodata->listCourseStaff($item['code']);
					$text = "";
					if ($staf['stat']===true) {
						foreach ($staf['list'] as $that) {
							if (intval($that['role'])===COURSE_ADMIN) {
								if ($text!=="") $text = $text.", ";
								$text = $text.$that['staffname'];
							}
						}
					}
					$show = $view->create_menu_item("selectstaff&code=".
						$item['code']."&alvl=".COURSE_ADMIN,
						'Assign',MENUITEM_CMD_);
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					if (strlen($text)>0)
						$tcol->insert_inner($text."&nbsp;&nbsp;");
					$tcol->append_object($show);
					$text = "";
					if ($staf['stat']===true) {
						foreach ($staf['list'] as $that) {
							if (intval($that['role'])===COURSE_STAFF) {
								if ($text!=="") $text = $text.",&nbsp;";
								$text = $text.$that['staffname'];
							}
						}
					}
					$show = $view->create_menu_item("selectstaff&code=".
						$item['code']."&alvl=".COURSE_STAFF,
						'Assign',MENUITEM_CMD_);
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					if (strlen($text)>0)
						$tcol->insert_inner($text."&nbsp;&nbsp;");
					$tcol->append_object($show);
				}
			}
		}
		else $view->insert_highlight('No course info found in database.');
		$view->insert_menu($list,false);
	}
	function sendCSV($template=false) {
		$head =  [ HEADER_COURSE_CODE,
			HEADER_COURSE_NAME, HEADER_COURSE_UNIT, HEADER_COURSE_ASEM ];
		$data = [];
		if ($template===false) {
			$cors = $this->_dodata->listCourses();
			if ($cors['stat']===true) {
				foreach ($cors['list'] as $item) {
					array_push($data,[$item['code'],
						$item['name'],$item['unit'],$item['asem']]);
				}
			}
		}
		require_once dirname(__FILE__).'/../include/FileText.php';
		$fcsv = new FileText();
		$fcsv->sendCSV('listcourse.csv',$head,$data);
	}
}
?>
