<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageStaffSelectCh extends PageDone {
	use TraitUniData;
	function __construct() {
		parent::__construct('Staff Selection for '.$_POST['cCode']);
		if (empty($_POST['cCoId'])||empty($_POST['cCode'])||
				empty($_POST['cType'])||empty($_POST['postSelectStaff'])) {
			$this->throw_debug('Invalid staff selection info?!');
		}
		// remove existing course assignment
		$coid = intval($_POST['cCoId']);
		$alvl = intval($_POST['cType']);
		$list = $this->_dodata->listCourseStaff($_POST['cCode']);
		//$this->throw_debug('List:'.json_encode($list));
		switch ($alvl) {
			case COURSE_ADMIN: $type = "Coordinator"; break;
			case COURSE_STAFF: $type = "Staff"; break;
			default:
				$this->throw_debug('Invalid assignment type! ('.$alvl.')');
		}
		$text = "<b>Assignment ($type) for ".$_POST['cCode']."</b><br><br>\n";
		$chk0 = []; $chk1 = [];
		if ($list['stat']===true) {
			foreach ($list['list'] as $item) {
				$item['coid'] = intval($item['coid']);
				$item['stid'] = intval($item['stid']);
				$item['role'] = intval($item['role']);
				if ($item['coid']!==$coid)
					$this->throw_debug('Invalid course-staff data!');
				$stid = $item['stid'];
				$clvl = $item['role'];
				//$text = $text."Checking {".$item['staffname']."}<br>\n";
				if ($clvl!==$alvl) continue;
				$clvl = COURSE_CHECK;
				array_push($chk0,$item);
				//$text = $text."Removing {".$item['staffname']."}<br>\n";
				//$this->_dodata->createCourseStaff($coid,$stid,$clvl);
			}
		}
		if (isset($_POST['pickStaff'])) {
			$size = count($_POST['pickStaff']);
			// update new course assignment
			foreach ($_POST['pickStaff'] as $staf) {
				$temp = $this->_dodata->findStaff($staf);
				if ($temp['stat']!==true)
					$this->throw_debug('Invalid staff!'.json_encode($temp));
				$stid = $temp['id'];
				array_push($chk1,$temp);
				//$text = $text."Adding {".$temp['nick']."}<br>\n";
				//$this->_dodata->createCourseStaff($coid,$temp['id'],$alvl);
				if ($alvl==COURSE_ADMIN) {
					if ($size>1)
						$text = $text."<br>## Only 1 admin! (".$size.")<br>\n";
					break;
				}
			}
		}
		foreach ($chk0 as $item) {
			$done = $item;
			$stid = $item['stid'];
			foreach ($chk1 as $look) {
				if ($stid==$look['id']) {
					// removed and added
					$look['id'] = 0;
					$done = null;
					break;
				}
			}
			if ($done!==null) {
				$text = $text."Removing {".$item['staffname']."}<br>\n";
				$this->_dodata->createCourseStaff($coid,$stid,COURSE_CHECK);
			}
		}
		foreach ($chk1 as $look) {
			if ($look['id']==0) continue;
			$anew = $look;
			foreach ($chk0 as $item) {
				if ($anew['id']===$item['stid']) {
					// already in
					$anew = null;
					break;
				}
			}
			if ($anew!==null) {
				$text = $text."Adding {".$anew['nick']."}<br>\n";
				$this->_dodata->createCourseStaff($coid,$look['id'],$alvl);
			}
		}
		$this->_dotext = $text."<br>Course assignment updated!";
	}
	function build_page() {
		parent::build_page();
	}
}
?>
