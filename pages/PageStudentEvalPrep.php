<?php
require_once dirname(__FILE__).'/PageDone.php';
class PageStudentEvalPrep extends PageDone {
	function __construct() {
		if (empty($_GET['code'])||empty($_GET['ssem'])) {
			$this->throw_debug('Invalid makeeval info!');
		}
		$code = $_GET['code'];
		$ssem = $_GET['ssem'];
		parent::__construct("Student Evaluation for $code-$ssem");
		$this->_dodata->makeEvalReady($code,$ssem);
		$this->_doskip = 1;
		$this->_dotext = "Course $code ready for student evaluation!";
	}
}
?>
