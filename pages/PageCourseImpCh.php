<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageCourseImpCh extends PageDone {
	use TraitUniData;
	function __construct() {
		parent::__construct();
		if (empty($_GET['cimp']))
			$this->throw_debug('Not enough info!');
		$test = $_GET['cimp'];
		$find = null;
		$list = $this->_dodata->listCourseImps();
		foreach ($list['list'] as $item) {
			if ($item['name']===$test) // @tbl_name?
				$find = $item;
		}
		if ($find===null) $this->throw_debug('Table not found?!');
		$part = explode('_',$test);
		$code = $part[0];
		$ssem = $part[1];
		$this->_dodata->removeCourseImps($code,$ssem);
		$this->_dotext = 'Course implementation removed!';
		$this->done_header($code." ".$ssem);
	}
}
?>
