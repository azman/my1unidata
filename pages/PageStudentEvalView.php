<?php
require_once dirname(__FILE__).'/PageData.php';
class PageStudentEvalView extends PageData {
	protected $_code;
	protected $_ssem;
	protected $_show;
	function __construct() {
		if (empty($_GET['code'])||empty($_GET['ssem']))
			$this->throw_debug('Invalid vieweval info!');
		parent::__construct();
		$this->_code = $_GET['code'];
		$this->_ssem = $_GET['ssem'];
		if (!isset($this->_show))
			$this->_show = false;
		$ccss = new CSSObject('css_chart');
		$text = <<<CCSS
.bar {
  fill: blue; /* changes the background */
  transition: fill .3s ease;
  cursor: pointer;
}
.bar text {
  color: black;
}
.gdat text {
  font: 10px sans-serif;
}
.gdat text.com {
  display: block;
}
.gdat:hover text.com {
  display: none;
}
.gdat text.alt {
  display: none;
}
.gdat:hover text.alt {
  display: block;
}
.bar:hover,
.bar:focus {
  fill: black;
}
.bar:hover text,
.bar:focus text {
  fill: blue;
}
.xchk text {
  //text-anchor: middle;
  font: 10px sans-serif;
}
CCSS;
		$ccss->insert_inner($text);
		$this->_head->append_object($ccss);
	}
	function build_page() {
		$show_who = $this->_show;
		$code = $this->_code;
		$ssem = $this->_ssem;
		$temp = $this->_dodata->selectSession($ssem);
		$user = $this->_dodata->getProfile();
		if ($user['type']===USER_ADMIN)
			$show_who = true;
		$cors = $this->_dodata->findCourse($code);
		if ($cors['stat']===false) {
			$this->throw_debug('Invalid course code! ('.$code.')');
		}
		$view = $this->_doview;
		$view->insert_page_title();
		$menu = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($menu);
		$view->insert_page_section("Student Evaluation for ".
			$code." ".$cors['name']." (".$temp.")");
		$staf = $this->_dodata->listCoursesStaffs(null,$code,$ssem);
		if ($staf['stat']!==true)
			$this->throw_debug('Cannot get staff?!'.json_encode($staf));
		// staff stat
		$staf_new = [ "id" => 0, "unid" => 0, "name" => null,
			"pbqz" => [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
				[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
				[0,0,0,0,0]],
			"pcqz" => [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
				[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
				[0,0,0,0,0],[0,0,0,0,0]],
			"bcnt" => 0, "ccnt" => 0, "self" => false,
			"chka" => 0, "chkb" => 0, "chkc" => 0,
			"over" => 0, "ocnt" => 1
		];
		$staf_sum = [];
		foreach ($staf['list'] as $item) {
			$staf_tmp = $staf_new;
			$staf_tmp['id'] = intval($item['stid']);
			$staf_tmp['unid'] = $item['staff'];
			$staf_tmp['name'] = $item['staffname'];
			$staf_tmp['nick'] = $item['staffnick'];
			if ($staf_tmp['unid']===$user['unid']) {
				$staf_tmp['self'] = true;
			}
			array_push($staf_sum,$staf_tmp);
		}
		//$this->throw_debug('[DEBUG] => '.json_encode($staf_sum));
		// prepare to get evaluation results
		$tabs = $code."_".$ssem;
		$eval = $tabs."_EVAL";
		$stat = $tabs."_EVALSTAT";
		$list = $this->_dodata->listCourseEvalStat($stat);
		if ($list['stat']!==true) {
			$this->throw_debug('Cannot get stat!');
		}
		$size = 0;
		foreach ($list['list'] as $item) {
			if ($item['stat']===EVAL_DONE) {
				$size++;
			}
		}
		// data array
		$paqx_sum = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]];
		$pbqx_sum = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0]];
		$pcqx_sum = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
			[0,0,0,0,0],[0,0,0,0,0]];
		$comm_sum = [];
		$temp = 0;
		// browse the list
		$list = $this->_dodata->listCourseEval($eval);
		foreach ($list['list'] as $item) {
			$stid = intval($item['stid']);
			$ldid = intval($item['ldid']);
			$schk = "Unknown";
			$lchk = "Unknown";
			// part a
			for ($loop=1;$loop<=9;$loop++) {
				$paqx = intval($item['paq'.$loop]);
				$paqx_sum[($loop-1)][($paqx-1)]++;
			}
			// part b
			$step = 0;
			foreach ($staf_sum as $staf) {
				if ($stid===$staf['id']) {
					$schk = $staf['nick'];
					break;
				}
				$step++;
			}
			for ($loop=1;$loop<=7;$loop++) {
				$pbqx = intval($item['pbq'.$loop]);
				$pbqx_sum[($loop-1)][($pbqx-1)]++;
				$staf_sum[$step]['pbqz'][($loop-1)][($pbqx-1)]++;
			}
			$staf_sum[$step]['bcnt']++;
			// part c
			$step = 0;
			foreach ($staf_sum as $staf) {
				if ($ldid===$staf['id']) {
					$lchk = $staf['nick'];
					break;
				}
				$step++;
			}
			for ($loop=1;$loop<=8;$loop++) {
				$pcqx = intval($item['pcq'.$loop]);
				$pcqx_sum[($loop-1)][($pcqx-1)]++;
				$staf_sum[$step]['pcqz'][($loop-1)][($pcqx-1)]++;
			}
			$staf_sum[$step]['ccnt']++;
			// comments
			$comm = trim($item['comm']);
			if ($comm === '-') $comm = null;
			if (!empty($comm)) {
				$comm_tmp = [ "txt" => $comm, "lec" => $schk, "lab" => $lchk ];
				array_push($comm_sum,$comm_tmp);
			}
			$temp++;
		}
		if ($temp!==$size)
			$view->insert_highlight("Inconsistent size?($size|$temp)");
		// create table
		$ttab = $view->create_table();
		$view->insert_table($ttab);
		$view->create_table_header_row($ttab);
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("Part");
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("Question");
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("Stat (".$temp.")");
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("Average");
		$tcol = $view->create_table_header_col($ttab);
		$tcol->insert_inner("Percentage");
		include_once dirname(__FILE__).'/../include/config-survey.php';
		// check part a
		$full = 0.0; $over = 0.0;
		for ($loop=1;$loop<=9;$loop++) {
			$view->create_table_data_row($ttab);
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$text = "A".$loop;
			$tcol->insert_inner($text);
			$tcol = $view->create_table_data_col($ttab);
			$tcol->insert_inner($opts_partA[$text]);
			// show stat - BEGIN
			$svgd = new SVG(null,100,100,true);
			$svgg = $svgd->create_g($svgd,null);
			$svgg->transform("translate(0,0)");
			$svgx = $svgd->create_g($svgg,"x axis");
			$svgx->transform("translate(0,20)");
			// loop through AND find max
			$dmax = 0; $fval = 0;
			$xlen = 15; $xoff = 1 + $xlen;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $paqx_sum[($loop-1)][($next-1)];
				if ($freq>$dmax) $dmax = $freq;
				$fval += $freq*$next;
				$dval[$next-1] = $freq;
			}
			$ymax = 70;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $paqx_sum[($loop-1)][($next-1)];
				$yval = !$dmax?0:intval($ymax*$freq/$dmax);
				// draw label
				$svgt = $svgd->create_g($svgx,"xchk");
				$svgt->transform("translate(".$xnxt.",".$ymax.")");
				$svgt->draw_text($next,null,"0","9",null,null);
				// draw bar
				$yoff = $ymax - $yval;
				$svgb = $svgd->create_g($svgx,"bar gdat");
				$svgb->draw_rectangle("bar",$xnxt-4,$yoff,$xlen,$yval);
				$svgb->draw_text($freq,null,$xnxt,$yoff,null,null);
			}
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_object($svgd);
			// show stat - END
			$fval = !$temp?0.0:floatval($fval)/$temp;
			$text = number_format($fval,2,'.','');
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$pval = $fval*20;
			$text = number_format($pval,2,'.','').'%';
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$full += $pval;
		}
		$view->create_table_data_row($ttab);
		$tcol = $view->create_table_data_col($ttab,
			[ "class"=>"w3-center" ]);
		$full /= 9; $over += $full;
		foreach ($staf_sum as &$staf) {
			$staf['chka'] = $full;
			$staf['over'] += $full;
		}
		$text = '<b>Average Part A</b>: '.number_format($full,2,'.','').'%';
		$tcol->insert_keyvalue('colspan','5',true);
		$tcol->insert_inner($text);
		// check part b
		$full = 0.0;
		for ($loop=1;$loop<=7;$loop++) {
			$view->create_table_data_row($ttab);
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$text = "B".$loop;
			$tcol->insert_inner($text);
			$tcol = $view->create_table_data_col($ttab);
			$tcol->insert_inner($opts_partB[$text]);
			// show stat - BEGIN
			$svgd = new SVG(null,100,100,true);
			$svgg = $svgd->create_g($svgd,null);
			$svgg->transform("translate(0,0)");
			$svgx = $svgd->create_g($svgg,"x axis");
			$svgx->transform("translate(0,20)");
			// loop through AND find max
			$dmax = 0; $fval = 0;
			$xlen = 15; $xoff = 1 + $xlen;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $pbqx_sum[($loop-1)][($next-1)];
				if ($freq>$dmax) $dmax = $freq;
				$fval += $freq*$next;
				$dval[$next-1] = $freq;
			}
			$ymax = 70;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $pbqx_sum[($loop-1)][($next-1)];
				$yval = !$dmax?0:intval($ymax*$freq/$dmax);
				// draw label
				$svgt = $svgd->create_g($svgx,"xchk");
				$svgt->transform("translate(".$xnxt.",".$ymax.")");
				$svgt->draw_text($next,null,"0","9",null,null);
				// draw bar
				$yoff = $ymax - $yval;
				$svgb = $svgd->create_g($svgx,"bar gdat");
				$svgb->draw_rectangle("bar",$xnxt-4,$yoff,$xlen,$yval);
				if (count($staf_sum)>1) {
					$text = "{";
					foreach ($staf_sum as &$staf) {
						$what = $staf['pbqz'][($loop-1)][($next-1)];
						if ($text!=="{") $text = $text."+";
						$text = $text."$what";
					}
					$text = $text."}";
				} else {
					$text = "$freq {alt}";
				}
				$svgb->draw_text($freq,"com",$xnxt,$yoff,null,null);
				$svgb->draw_text($text,"alt",$xnxt,-10,null,null);
			}
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_object($svgd);
			// show stat - END
			$fval = !$temp?0.0:floatval($fval)/$temp;
			$text = number_format($fval,2,'.','');
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$pval = $fval*20;
			$text = number_format($pval,2,'.','').'%';
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$full += $pval;
		}
		$view->create_table_data_row($ttab);
		$tcol = $view->create_table_data_col($ttab,
			[ "class"=>"w3-center" ]);
		$full /= 7; $over += $full;
		foreach ($staf_sum as &$staf) {
			if ($staf['bcnt']>0) {
				$full_self = 0.0;
				for ($loop=1;$loop<=7;$loop++) {
					$fval = 0;
					for ($next=5;$next>0;$next--) {
						$freq = $staf['pbqz'][($loop-1)][($next-1)];
						$fval += $freq*$next;
					}
					$fval = floatval($fval)/$staf['bcnt'];
					$pval = $fval*20;
					$full_self += $pval;
				}
				$full_self /= 7;
				$staf['chkb'] = $full_self;
				$staf['over'] += $full_self;
				$staf['ocnt']++;
			}
		}
		$text = '<b>Average Part B</b>: '.number_format($full,2,'.','').'%';
		$tcol->insert_keyvalue('colspan','5',true);
		foreach ($staf_sum as &$staf) {
			$text = $text."<br>[".$staf['name'].":".
				number_format($staf['chkb'],2,'.','').'%';
			$text = $text.'('.$staf['bcnt'].')]';
		}
		$tcol->insert_inner($text);
		// check part c
		$full = 0.0;
		for ($loop=1;$loop<=8;$loop++) {
			$view->create_table_data_row($ttab);
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$text = "C".$loop;
			$tcol->insert_inner($text);
			$tcol = $view->create_table_data_col($ttab);
			$tcol->insert_inner($opts_partC[$text]);
			// show stat - BEGIN
			$svgd = new SVG(null,100,100,true);
			$svgg = $svgd->create_g($svgd,null);
			$svgg->transform("translate(0,0)");
			$svgx = $svgd->create_g($svgg,"x axis");
			$svgx->transform("translate(0,20)");
			// loop through AND find max
			$dmax = 0; $fval = 0;
			$xlen = 15; $xoff = 1 + $xlen;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $pcqx_sum[($loop-1)][($next-1)];
				if ($freq>$dmax) $dmax = $freq;
				$fval += $freq*$next;
				$dval[$next-1] = $freq;
			}
			$ymax = 70;
			for ($next=5,$xnxt=5;$next>0;$next--,$xnxt+=$xoff) {
				$freq = $pcqx_sum[($loop-1)][($next-1)];
				$yval = !$dmax?0:intval($ymax*$freq/$dmax);
				// draw label
				$svgt = $svgd->create_g($svgx,"xchk");
				$svgt->transform("translate(".$xnxt.",".$ymax.")");
				$svgt->draw_text($next,null,"0","9",null,null);
				// draw bar
				$yoff = $ymax - $yval;
				$svgb = $svgd->create_g($svgx,"bar gdat");
				$svgb->draw_rectangle("bar",$xnxt-4,$yoff,$xlen,$yval);
				if (count($staf_sum)>1) {
					$text = "{";
					foreach ($staf_sum as &$staf) {
						$what = $staf['pcqz'][($loop-1)][($next-1)];
						if ($text!=="{") $text = $text."+";
						$text = $text."$what";
					}
					$text = $text."}";
				} else {
					$text = "$freq {alt}";
				}
				$svgb->draw_text($freq,"com",$xnxt,$yoff,null,null);
				$svgb->draw_text($text,"alt",$xnxt,-10,null,null);
			}
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_object($svgd);
			// show stat - END
			$fval = !$temp?0.0:floatval($fval)/$temp;
			$text = number_format($fval,2,'.','');
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$pval = $fval*20;
			$text = number_format($pval,2,'.','').'%';
			$tcol = $view->create_table_data_col($ttab,
				[ "class"=>"w3-center" ]);
			$tcol->insert_inner($text);
			$full += $pval;
		}
		$view->create_table_data_row($ttab);
		$tcol = $view->create_table_data_col($ttab,
			[ "class"=>"w3-center" ]);
		$full /= 8; $over += $full;
		foreach ($staf_sum as &$staf) {
			if ($staf['ccnt']>0) {
				$full_self = 0.0;
				for ($loop=1;$loop<=8;$loop++) {
					$fval = 0;
					for ($next=5;$next>0;$next--) {
						$freq = $staf['pcqz'][($loop-1)][($next-1)];
						$fval += $freq*$next;
					}
					$fval = floatval($fval)/$staf['ccnt'];
					$pval = $fval*20;
					$full_self += $pval;
				}
				$full_self /= 8;
				$staf['chkc'] = $full_self;
				$staf['over'] += $full_self;
				$staf['ocnt']++;
			}
		}
		$text = '<b>Average Part C</b>: '.number_format($full,2,'.','').'%';
		$tcol->insert_keyvalue('colspan','5',true);
		foreach ($staf_sum as &$staf) {
			$text = $text."<br>[".$staf['name'].":".
				number_format($staf['chkc'],2,'.','').'%';
			$text = $text.'('.$staf['bcnt'].')]';
		}
		$tcol->insert_inner($text);
		// overall
		$view->create_table_data_row($ttab);
		$tcol = $view->create_table_data_col($ttab,
			[ "class"=>"w3-center" ]);
		$tcol->insert_keyvalue('colspan','5',true);
		$over /= 3;
		$text = '<b>Average Overall</b>: '.number_format($over,2,'.','').'%';
		foreach ($staf_sum as &$staf) {
			$staf['over'] /= $staf['ocnt'];
			$text = $text."<br>[".$staf['name'].":".
				number_format($staf['over'],2,'.','').'%]';
		}
		$tcol->insert_inner($text);
		$view->insert_menu($menu);
		// comments
		if (count($comm_sum)>0) {
			// create table
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			$view->create_table_header_row($ttab);
			$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
			$colw = ($show_who===true) ? '5' : '3';
			$tcol->insert_keyvalue('colspan',$colw,true);
			$tcol->insert_inner("Student Comments");
			if ($show_who===true) {
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner("LEC");
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner("LAB");
			}
			$loop = 0;
			foreach ($comm_sum as $comm) {
				$loop++;
				$view->create_table_data_row($ttab);
				$tcol = $view->create_table_data_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_style("width:10%;");
				$tcol->insert_inner($loop);
				$tcol = $view->create_table_data_col($ttab);
				$colw = ($show_who===true) ? '2' : '4';
				$tcol->insert_keyvalue('colspan',$colw,true);
				$tcol->insert_inner($comm['txt']);
				if ($show_who===true) {
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($comm['lec']);
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($comm['lab']);
				}

			}
		}
		$view->insert_menu($menu);
	}
}
?>
