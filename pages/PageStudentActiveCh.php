<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageStudentActiveCh extends PageDone {
	use TraitUniData;
	function __construct() {
		switch ($_GET['do']) {
			case "reactiv8":
				$flag = STUDENT_FLAG_ACTIVE;
				$mesg = "Student reactivated";
				break;
			case "deactiv8": default:
				$flag = STUDENT_FLAG_DROP;
				$mesg = "Student deactivated";
				break;
		}
		$from = $_GET['from'];
		$mesg = $mesg." in ".$from;
		$stid = intval($_GET['stid']);
		parent::__construct($from." Update");
		$this->_dodata->modifyCourseStudent($from,$stid,$flag);
		$this->_dotext = $mesg;
		$this->_doskip = 1;
	}
}
?>
