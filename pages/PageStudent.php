<?php
require_once dirname(__FILE__).'/../include/TraitBase.php';
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/Page.php';
class PageStudent extends Page {
	use TraitBase;
	use TraitUniData;
	function __construct() {
		$this->_title_ = 'Student List';
		parent::__construct();
		$this->initialize_base(LOGIN_MODE,DEFAULT_DATA_CLASS);
	}
	function build_page() {
		$user = $this->_dodata->getProfile();
		$stud = $this->_dodata->listStudents();
		$view = $this->_doview;
		$view->insert_page_title();
		// create return link & csv download link
		$menu = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($menu);
		// create table for list of students
		if ($stud['stat']==true) $size = count($stud['list']);
		else $size = 0;
		if ($size>0) {
			$chk0 = 0;
			foreach ($stud['list'] as $item) {
				$flag = $this->_dodata->isFlagUser($item['unid']);
				if ($flag&FLAG_INACTIVE) $chk0++;
			}
			$view->insert_highlight("<i>Student Count</i>: ".
				($size-$chk0)." / ".$size);
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			$view->create_table_header_row($ttab);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STUDENT_UNID);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STUDENT_NRIC);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STUDENT_NAME);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STUDENT_PROG);
			foreach ($stud['list'] as $item) {
				$flag = $this->_dodata->isFlagUser($item['unid']);
				if ($flag&FLAG_INACTIVE) continue;
				$view->create_table_data_row($ttab);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['unid']."&nbsp;&nbsp;");
				if ($user['type']===USER_ADMIN) {
					$show = $view->create_menu_item("rstpass&unid=".
						$item['unid'],"Reset Password",
						MENUITEM_CMD_,[ "color" => "w3-red w3-small" ]);
					$tcol->append_object($show);
					$show = $view->create_menu_item("disableStud&stid=".
						$item['unid'],"Disable",
						MENUITEM_CMD_,[ "color" => "w3-red w3-small" ]);
					$tcol->append_object($show);
				}
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['bkid']);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['name']);
				$tcol = $view->create_table_data_col($ttab);
				$item['type'] = intval($item['flag']);
				$pchk = $this->_dodata->findProg(null,$item['type']);
				$tcol->insert_inner($pchk['prog']);
			}
			if ($chk0>0) {
				$ttab = $view->create_table();
				$view->insert_table($ttab);
				$view->create_table_header_row($ttab);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_UNID);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_NRIC);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_NAME);
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STUDENT_PROG);
				foreach ($stud['list'] as $item) {
					$flag = $this->_dodata->isFlagUser($item['unid']);
					if (!($flag&FLAG_INACTIVE)) continue;
					$view->create_table_data_row($ttab);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['unid']."&nbsp;&nbsp;");
					if ($user['type']===USER_ADMIN) {
						$show = $view->create_menu_item("rstpass&unid=".
							$item['unid'],"Reset Password",
							MENUITEM_CMD_,[ "color" => "w3-red w3-small" ]);
						$tcol->append_object($show);
						$show = $view->create_menu_item("enableStud&stid=".
							$item['unid'],"Enable",
							MENUITEM_CMD_,[ "color" => "w3-green w3-small" ]);
						$tcol->append_object($show);
					}
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['bkid']);
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['name']);
					$tcol = $view->create_table_data_col($ttab);
					$item['type'] = intval($item['flag']);
					$pchk = $this->_dodata->findProg(null,$item['type']);
					$tcol->insert_inner($pchk['prog']);
				}
			}
		}
		else $view->insert_highlight('No student profile found in database.');
		$view->insert_menu($menu);
	}
	function sendCSV() {
		$stud = $this->_dodata->listStaffs();
		$head =  [ HEADER_STAFF_UNID,
			HEADER_STAFF_NRIC, HEADER_STAFF_NAME, HEADER_STAFF_NICK ];
		$data = [];
		foreach ($stud['list'] as $item) {
			if ($item['unid']===DEFAULT_ROOT_UNID) continue;
			array_push($data,[$item['unid'],$item['bkid']
				,$item['name'],$item['nick']]);
		}
		require_once dirname(__FILE__).'/FileText.php';
		$fcsv = new FileText();
		$fcsv->sendCSV('liststaff.csv',$head,$data);
	}
}
?>
