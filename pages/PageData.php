<?php
require_once dirname(__FILE__).'/../include/TraitBase.php';
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/Page.php';
class PageData extends Page {
	use TraitBase;
	use TraitUniData;
	function __construct($title=null) {
		$this->_validate = true; // NOT configurable
		if ($title!==null) $this->_title_ = $title;
		parent::__construct();
		$this->initialize_base(LOGIN_MODE,DEFAULT_DATA_CLASS);
	}
}
?>
