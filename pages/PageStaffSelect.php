<?php
require_once dirname(__FILE__).'/PageData.php';
class PageStaffSelect extends PageData {
	protected $_code;
	protected $_alvl;
	function __construct() {
		if (!isset($_GET['code'])||!isset($_GET['alvl']))
			$this->throw_debug("Invalid staff-course!");
		$code = $_GET['code'];
		$alvl = intval($_GET['alvl']);
		if ($alvl===COURSE_ADMIN) $text = "Coordinator";
		else if ($alvl===COURSE_STAFF) $text = "Staff";
		else $text = "Unknown";
		$this->_code = $code;
		$this->_alvl = $alvl;
		parent::__construct('Assign '.$text.' for '.$code);
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function post_check() {
	var chk_form = document.getElementById('form_course');
	chk_form.cCode.disabled = false;
	return true;
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		if ($this->_code==null||$this->_alvl==null)
			$this->throw_debug('No info!');
		$code = $this->_code;
		$alvl = $this->_alvl;
		$user = $this->_dodata->getProfile();
		$item = $this->_dodata->findCourse($code);
		if ($item['stat']==false)
			$this->throw_debug('Invalid course code?!');
		// base build
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		// create back link
		$list = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($list);
		// create form
		$form = $view->create_form('form_course');
		$form->insert_onsubmit('javascript:return post_check();');
		$view->create_form_input_hidden($form,'cCoId',null,$item['id']);
		$view->create_form_input_hidden($form,'cType',null,$alvl);
		$view->create_form_input_text($form,'Course Code','cCode',
			[ "tval"=>$code , "ro"=>1 , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Course Name','cName',
			[ "tval"=>$item['name'] , "ro"=>1 , "linebr"=>"1" ]);
		// selection for staff
		$text = 'Select Staff';
		if ($alvl===COURSE_ADMIN)
			$text = $text." (Only 1 - Ctrl+Click to deselect)";
		else $text = $text." (Ctrl+Click to select multiple)";
		$temp = $view->create_form_select($form,
			$text,'pickStaff[]',[],["linebr"=>2]);
		$temp->insert_keyvalue("size","10",true);
		// create options here
		$size = 0;
		$staf = $this->_dodata->listStaffs();
		foreach ($staf['list'] as $item) {
			if ($item['unid']===DEFAULT_ROOT_UNID) continue;
			$unid = $item['unid'];
			$test = null;
			$slst = $this->_dodata->listCourseStaff($code,$unid,$alvl);
			if ($slst['stat']===true) {
				$test = $slst['list'][0];
				$test['role'] = intval($test['role']);
			}
			$chkd = ($test!==null&&$test['role']===$alvl) ? true : false;
			$view->create_form_select_option($form,$temp,
				$item['name'],$item['unid'],$chkd);
			$size++;
		}
		$temp->insert_constant('multiple');
		$temp->insert_keyvalue('size',$size);
		$temp->insert_keyvalue('style','overflow-y:auto');
		$view->create_form_submit($form,'Submit','postSelectStaff');
		$view->insert_form($form);
		$view->insert_menu($list);
	}
}
?>
