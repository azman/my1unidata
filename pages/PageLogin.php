<?php
require_once dirname(__FILE__).'/../include/TraitLogin.php';
require_once dirname(__FILE__).'/Page.php';
class PageLogin extends Page {
	use TraitLogin;
	function __construct($title='Login') {
		$this->_title_ = $title;
		parent::__construct();
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_sha512lib();
		$this->append_2head($temp);
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		// create login form
		$form = $view->create_form('form_login');
		$form->insert_onsubmit('javascript:return post_check();');
		$temp = $view->create_form_input_text($form,'User ID','username',
			[ "hold"=>'User ID' , "linebr" => "1" ]);
		$temp->insert_constant("required");
		$temp = $view->create_form_input_pass($form,'Password','password',
			[ "hold"=>'Password' , "linebr" => "1" ]);
		$temp->insert_constant("required");
		$view->create_form_submit($form,'Login','postLogin');
		$view->insert_form($form,["class"=>"w3-margin-top"]);
	}
}
?>
