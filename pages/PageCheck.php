<?php
require_once dirname(__FILE__).'/PageData.php';
class PageCheck extends PageData {
	function __construct() {
		parent::__construct();
	}
	function build_page() {
		$view = $this->_doview;
		$view->insert_page_title();
		// customize main page
		$user = $this->_dodata->getProfile();
		// custom main page
		$list = $view->menu_list_item_command(null,"donick","Change Nickname");
		$list = $view->menu_list_item_command($list,"dopass","Change Password");
		$list = $view->menu_list_item_link($list,"logout.php","Logout");
		$list = $view->menu_list_item_link($list,"work.php","Home");
		//$user = $this->_validate?$this->_dodata->getProfile():null;
		if (!$this->_validate) $user = null;
		$view->insert_menu($list,$user);
		if (!$this->_dodata->is_valid_user())
		//if (!isset($user['staf']))
			$this->throw_debug('Invalid user?!');
		// present info/options
		//if ($user['staf']===true) {
		if ($this->_dodata->is_staff()) {
			// staff options
			$this->_dodata->checkCoursesStaffs();
			$list = $this->_dodata->listCoursesStaffs($user['unid']);
			if ($list['stat']===true) {
				$step = 0;
				$temp = intval(date("Y"));
				$lchk = null;
				foreach ($list['list'] as $item) {
					$item['role'] = intval($item['role']);
					if ($item['role']===COURSE_CHECK) continue;
					$dbug = intval(substr($item['ssem'],0,4));
					if ($temp-$dbug>3) break;
					$lref = "impcourse&ssem=".$item['ssem'].
						"&code=".$item['course'];
					$llbl = $item['ssem']." - ".$item['course']." (".
						$item['coursename'].")";
					if ($step===0)
						$view->insert_page_section("Course Implementations");
					$lchk = $view->menu_list_item_command($lchk,$lref,$llbl);
					$step++;
				}
				if ($lchk!==null) {
					$menu = $view->insert_menu($lchk,false);
					$menu->insert_class_2child("w3-margin-bottom","button");
				}
			}
			// staf header
			$view->insert_page_section("Create Course Implementation");
			// create form
			$form = $view->create_form('form_command');
			$form->insert_keyvalue('enctype','multipart/form-data');
			$view->create_form_input_hidden($form,'unid',null,$user['unid']);
			// selection for course
			$list = [];
			$this->_dodata->checkCourseStaff();
			$cors = $this->_dodata->listCourseStaff(null,$user['unid'],null);
			if ($cors['stat']===true) {
				foreach ($cors['list'] as $item) {
					$item['role'] = intval($item['role']);
					if ($item['role']!==COURSE_ADMIN) continue;
					array_push($list,[$item['course']." - ".$item['coursename'],
						$item['course'],false]);
				}
			}
			if (count($list)===0)
				array_push($list,["No Courses to Coordinate","",false]);
			$view->create_form_select($form,'Select Course','pickCourse',
				$list,["linebr"=>2]);
			// selection for semester
			$list = array();
			date_default_timezone_set('UTC');
			$temp = explode(':',date("Y:m"));
			$year = intval(array_shift($temp));
			$mmon = intval(array_shift($temp)); // used to select default??
			for ($loop=0;$loop<2;$loop++) {
				$temp = 'Academic Session '.$year.'/'.($year+1).' Semester 2';
				array_push($list,[$temp,$year.''.($year+1).'2',true]);
				$temp = 'Academic Session '.$year.'/'.($year+1).' Semester 1';
				array_push($list,[$temp,$year.''.($year+1).'1',true]);
				$year--;
			}
			$view->create_form_select($form,
				'Select Academic Session / Semester','pickSem',
				$list,["linebr"=>2]);
			$view->create_form_input_hidden($form,'aCmd',
				null,TASK_STAFF_EXECUTE_COURSE);
			// link to get template
			$item = $view->create_menu_item('ccompcsv',
				'Template',MENUITEM_CMD_,[ "class" => "w3-small" ]);
			$temp = $view->create_form_input_file($form,
				'Select Data File (CSV) ','dataFile',["linebr"=>1]);
			$temp->get_label()->append_object($item);
			$mind = $view->create_form_label($form,
				'Please Select Correct Session/Semester!');
			$mind->insert_linebr();
			$view->create_form_submit($form,'Implement Course','postCourseImp');
			$view->insert_form($form,[ "class" => "w3-margin-left",
				"fwpct" => "35" ]);
			// staff links
			$list = $view->menu_list_item_command(null,"viewstaff","Staffs");
			$list = $view->menu_list_item_command($list,
				"viewstudent","Students");
			$list = $view->menu_list_item_command($list,"viewcourse","Courses");
			$list = $view->menu_list_item_command($list,
				"viewsemcourse","Implementations");
			$temp = $view->insert_menu($list);
		}
		else {
			if ($user['type']!==USER_STUDENT)
				$this->throw_debug('Unknown user type?!');
			// student options
			$view->insert_page_section("List of Course(s) Taken:");
			date_default_timezone_set('UTC');
			$temp = explode(':',date("Y:m"));
			$year = intval(array_shift($temp));
			for ($loop=0;$loop<6;$loop++) {
				$nsem = ($loop%2==0)?2:1;
				$temp = 'Academic Session '.
					$year.'/'.($year+1).' Semester '.$nsem;
				$ssem = $year.''.($year+1).''.$nsem;
				// get it!
				$test = $this->_dodata->findStudentCourses($user['id'],$ssem);
				if ($test['stat']===true) {
					if (count($test['list'])>0) {
						$this->_dodata->selectSession($ssem);
						$view->insert_highlight($temp);
						$step = 0;
						foreach ($test['list'] as $item) {
							$code = $item['code'];
							$name = $item['name'];
							$text = "$code - $name";
							$that = $view->insert_block($text);
							$temp = $this->_dodata->testEvalReady($code,$ssem);
							if ($temp===EVAL_INIT) {
								$echk = $view->create_menu_item(
									"studeval&code=".$code."&ssem=".$ssem,
									"Evaluate",MENUITEM_CMD_,
									[ "class" => "w3-small" ]);
								$that->insert_object($echk);
							}
							else if ($temp===EVAL_DONE) {
								//$etmp = $this->make_span("Evaluated");
								//$etmp->insert_style("color:green;");
								$etmp = $view->create_badge("Evaluated",
									["class"=>"w3-green"]);
								$etmp->insert_style("color:green;");
								$that->insert_object($etmp);
							}
							$cors = $this->_dodata->
								listCoursesComponents($item['coid']);
							// create table
							$ttab = $view->create_table();
							$view->insert_table($ttab);
							$view->create_table_header_row($ttab);
							foreach ($cors['list'] as $what) {
								$tcol = $view->create_table_header_col($ttab,
									[ "class"=>"w3-center" ]);
								$tcol->insert_inner($what['lbl'].
									" (".$what['pct'].")");
							}
							// info
							$view->create_table_data_row($ttab);
							foreach ($cors['list'] as $mark) {
								$temp = strtolower($mark['name']);
								$tcol = $view->create_table_data_col($ttab,
									[ "class"=>"w3-center" ]);
								if ($tcol->get_class()===null) {
									$tcol->insert_style("text-align:center;");
								}
								if ($item[$temp]!==null) {
									$temp = floatval($item[$temp]);
									$tpct = $temp*100.0/$mark['raw'];
									$show = DataCourse::get_grade($tpct);
								}
								else $show = "-";
								$tcol->insert_inner($show);
							}
						}
					}
				}
				// prev year if curr done
				if ($nsem==1) $year--;
			}
		}
	}
}
?>
