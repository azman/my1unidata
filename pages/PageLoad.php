<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/../include/TraitUpload.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageLoad extends PageDone {
	use TraitUpload;
	use TraitUniData;
	function __construct() {
		parent::__construct();
		// prepare page
		$this->_doskip = 1;
		$this->_dotext = "Data upload done.<br>";
		$this->upload();
		$csvd = $this->_csvd;
		$step = 0; $emsg = ""; $imsg = "";
		// what's the command?
		switch ($_POST['aCmd']) {
			case TASK_STAFF_CREATE_STAFF: {
				// find name and unid columns
				$iunid = -1; $ibkid = -1; $iname = -1; $inick = -1; $count = 0;
				foreach ($csvd['headline'] as $head) {
					$temp = strtoupper($head);
					if ($temp==HEADER_STAFF_UNID) $iunid = $count;
					else if ($temp==HEADER_STAFF_NRIC) $ibkid = $count;
					else if ($temp==HEADER_STAFF_NAME) $iname = $count;
					else if ($temp==HEADER_STAFF_NICK) $inick = $count;
					$count++;
				}
				if ($iunid<0||$ibkid<0||$iname<0||$inick<0) {
					$this->throw_debug('Invalid format?!('.
						$iunid.','.$ibkid.','.$iname.','.$inick.')');
				}
				$this->_dodata->checkStaffs();
				$step++; // first data line is AFTER header!
				foreach ($csvd['dataline'] as $line) {
					$step++;
					if (empty($line[$iunid])||empty($line[$ibkid])||
							empty($line[$iname]||empty($line[$inick]))) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Empty fields! ".
							json_encode($line)."<br>\n";
						continue;
					}
					$unid = strtoupper(trim($line[$iunid]));
					if (strlen($unid)<STAFF_UNID_SIZE) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid staff unid size! (".
							$unid.")<br>\n";
						continue;
					}
					if (!ctype_digit($unid)) { // must be pure numeric id
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid staff unid! (".$unid.")<br>\n";
						continue;
					}
					$nrid = strtoupper(trim($line[$ibkid]));
					if (strlen($nrid)<STAFF_NRIC_SIZE||
							!$this->validate_nrid($nrid)) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid staff nrid! (".$nrid.")<br>\n";
						continue;
					}
					$name = strtoupper(trim($line[$iname]));
					$nick = trim($line[$inick]);
					$staf = $this->_dodata->findStaff($unid);
					if ($staf['stat']==false) {
						$this->_dodata->createStaff($unid,$name,$nrid,$nick);
						$staf = $this->_dodata->findStaff($unid);
						if ($staf['stat']==false) {
							$emsg = $emsg."** Line ".$step.": ";
							$emsg = $emsg."Failed to add user '".
								$nick."'!<br>\n";
						}
						else {
							$imsg = $imsg."## Line ".$step.": ";
							$imsg = $imsg."User '".$nick."' created!<br>\n";
						}
					}
					else {
						//ignore existing
						//$imsg = $imsg."## Line ".$step.": ";
						//$imsg = $imsg."User '".$nick."' exists!<br>\n";
					}
				}
				break;
			}
			case TASK_STAFF_CREATE_COURSE: {
				// find name and unid columns
				$icode = -1; $iname = -1; $iunit = -1; $iasem = -1; $count = 0;
				foreach ($csvd['headline'] as $head) {
					$temp = strtoupper($head);
					if ($temp==HEADER_COURSE_CODE) $icode = $count;
					else if ($temp==HEADER_COURSE_NAME) $iname = $count;
					else if ($temp==HEADER_COURSE_UNIT) $iunit = $count;
					else if ($temp==HEADER_COURSE_ASEM) $iasem = $count;
					$count++;
				}
				if ($icode<0||$iname<0||$iunit<0||$iasem<0) {
					$this->throw_debug('Invalid format?!('.
						$icode.','.$iname.','.$iunit.','.$iasem.')');
				}
				$this->_dodata->checkCourses();
				foreach ($csvd['dataline'] as $line) {
					$step++;
					if (empty($line[$icode])||empty($line[$iname])||
							empty($line[$iunit])) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Empty fields! ".
							json_encode($line)."<br>\n";
						continue;
					}
					$code = strtoupper(trim($line[$icode]));
					$name = strtoupper(trim($line[$iname]));
					$unit = strtoupper(trim($line[$iunit]));
					$asem = strtoupper(trim($line[$iasem]));
					$cors = $this->_dodata->findCourse($code);
					if ($cors['stat']==false) {
						$this->_dodata->createCourse($code,$name,$unit,$asem);
						$cors = $this->_dodata->findCourse($code);
						if ($cors['stat']==false) {
							$emsg = $emsg."** Line ".$step.": ";
							$emsg = $emsg."Failed to add course '".
								$name."'@'".$code."'!<br>\n";
						} else {
							$imsg = $imsg."## Line ".$step.": ";
							$imsg = $imsg."Course '".$name.
								"'@'".$code."' created!<br>\n";
						}
					} //ignore existing
				}
				break;
			}
			case TASK_STAFF_EXECUTE_COURSE: {
				// find name and unid columns
				$iname = -1; $iraw_ = -1; $ipct_ = -1; $ilbl_ = -1;
				$igrp_ = -1; $isub_ = -1; $iidx_ = -1; $count = 0;
				foreach ($csvd['headline'] as $head) {
					$temp = strtoupper($head);
					if ($temp==HEADER_CCOMP_NAME) $iname = $count;
					else if ($temp==HEADER_CCOMP_RAW_) $iraw_ = $count;
					else if ($temp==HEADER_CCOMP_PCT_) $ipct_ = $count;
					else if ($temp==HEADER_CCOMP_LABEL) $ilbl_ = $count;
					else if ($temp==HEADER_CCOMP_GROUP) $igrp_ = $count;
					else if ($temp==HEADER_CCOMP_SUBGRP) $isub_ = $count;
					else if ($temp==HEADER_CCOMP_INDEX) $iidx_ = $count;
					$count++;
				}
				if ($iname<0||$iraw_<0||$ipct_<0||$ilbl_<0||
						$igrp_<0||$isub_<0||$iidx_<0) {
					$this->throw_debug('Invalid format?!('.
						$iname.','.$iraw_.','.$ipct_.','.$ilbl_.','.
						$igrp_.','.$isub_.','.$iidx_.')');
				}
				if (!isset($_POST["pickSem"])||!isset($_POST["pickCourse"]))
					$this->throw_debug('Incomplete post?!');
				// validate data
				$full = 0.0;
				foreach ($csvd['dataline'] as $line) {
					if (empty($line[$iname])||empty($line[$iraw_])
							||empty($line[$ipct_])||empty($line[$ilbl_])
							||empty($line[$igrp_])) {
						$this->throw_debug('Empty fields!');
					}
					if (!preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/',
							$line[$iname])) {
						// the string is not valid
						$this->throw_debug('Invalid column name?! ('.
							$line[$iname].')');
					}
					if (strtoupper(trim($line[$iname]))==="AS")
						$this->throw_debug('Cannot use sql keyword as name!');
					$full += floatval($line[$ipct_]);
				}
				if ($full!==100.0)
					$this->throw_debug('Total evaluation is not 100%?!');
				// prepare to insert
				$unid = $_POST["unid"];
				$code = $_POST["pickCourse"];
				$cors = $this->_dodata->findCourse($code);
				if ($cors['stat']==false)
					$this->throw_debug('Course not found!');
				$coid = $cors['id'];
				// validate
				$this->_dodata->checkCourseStaff();
				$alvl = COURSE_ADMIN;
				$test = $this->_dodata->listCourseStaff($code,$unid,$alvl);
				if ($test['stat']!==true)
					$this->throw_debug('Not a coordinator!');
				// select sem
				$ssem = $_POST["pickSem"];
				$tsem = intval($ssem%10);
				// check if correct semester
				if ($tsem==1) {
					if (!($cors['asem']&COURSE_SEMESTER1)) {
						$this->throw_debug($code.
							' not assigned to semester 1!');
					}
				} else if ($tsem==2) {
					if (!($cors['asem']&COURSE_SEMESTER2)) {
						$this->throw_debug($code.
							' not assigned to semester 2!');
					}
				}
				$this->_dodata->selectSession($ssem);
				// check if already implemented
				$this->_dodata->checkCoursesStaffs();
				$list = $this->_dodata->listCoursesStaffs(null,$code,$ssem);
				if ($list['stat']==true) {
					$pick = null;
					foreach($list['list'] as $data) {
						if (intval($data['role'])!==COURSE_CHECK) {
							$pick = $data;
							break;
						}
					}
					if ($pick!==null)
						$this->throw_debug('Already implemented?!');
				}
				// get list of staffs
				$list = $this->_dodata->listCourseStaff($code);
				if ($list['stat']!==true)
					$this->throw_debug('No staff assigned?!');
				foreach ($list['list'] as $item) {
					$stid = intval($item['stid']);
					$role = intval($item['role']);
					$this->_dodata->createCoursesStaffs($coid,
						$stid,$ssem,$role);
				}
				$this->_dodata->checkStudents();
				$this->_dodata->checkCoursesComponents();
				foreach ($csvd['dataline'] as $line) {
					$name = strtoupper(trim($line[$iname]));
					$raw = floatval($line[$iraw_]);
					$pct = floatval($line[$ipct_]);
					$lab = strtoupper(trim($line[$ilbl_]));
					$grp = intval($line[$igrp_]);
					$sub = intval($line[$isub_]);
					$idx = intval($line[$iidx_]);
					$this->_dodata->createCoursesComponents($cors['id'],
						$name,$raw,$pct,$lab,$grp,$sub,$idx);
				}
				$this->done_header($code."'@'".$ssem);
				$imsg = $imsg."Course '".$code."'@'".$ssem."' created!<br>\n";
				break;
			}
			case TASK_STAFF_ADD_STUDENTS: {
				// find name and unid columns
				$iunid = -1; $iname = -1; $inrid = -1; $iprog = -1;
				$ilgrp = -1; $imgrp = -1; $iflag = -1; $count = 0;
				foreach ($csvd['headline'] as $head) {
					$temp = strtoupper($head);
					if ($temp==HEADER_STUDENT_UNID) $iunid = $count;
					else if ($temp==HEADER_STUDENT_NAME) $iname = $count;
					else if ($temp==HEADER_STUDENT_NRIC) $inrid = $count;
					else if ($temp==HEADER_STUDENT_PROG) $iprog = $count;
					else if ($temp==HEADER_STUDENT_LGRP) $ilgrp = $count;
					else if ($temp==HEADER_STUDENT_MGRP) $imgrp = $count;
					else if ($temp==HEADER_STUDENT_FLAG) $iflag = $count;
					$count++;
				}
				if ($iunid<0||$iname<0||$inrid<0||$iprog<0||
						$ilgrp<0||$imgrp<0) {
					$this->throw_debug('Invalid format?!('.
						$iunid.','.$iname.','.$inrid.','.$iprog.','.
						$ilgrp.','.$imgrp.')');
				}
				if (!isset($_POST["pickSem"])||!isset($_POST["pickCourse"]))
					$this->throw_debug('Incomplete post?!');
				$ssem = strtoupper($_POST["pickSem"]);
				$code = strtoupper($_POST["pickCourse"]);
				$cors = $this->_dodata->findCourse($code);
				$table = $code.'_'.$ssem;
				$this->_dodata->checkStudents();
				$this->_dodata->selectSession($_POST["pickSem"]);
				$this->_dodata->checkCourseStudent($table,$cors['id']);
				foreach ($csvd['dataline'] as $line) {
					$step++;
					if (empty($line[$iunid])||empty($line[$iname])||
							empty($line[$inrid])||empty($line[$iprog])) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Empty fields! ".
							json_encode($line)."<br>\n";
						continue;
					}
					$unid = strtoupper(trim($line[$iunid]));
					$unid = $this->remove_nonascii($unid);
					if (strlen($unid)<STUDENT_UNID_SIZE) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid student unid size! (".
							$unid.")<br>\n";
						continue;
					}
					$uchk = substr($unid,0,STUDENT_UNID_SIZE);
					if (!ctype_digit($uchk)) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid student unid! (".$unid.")<br>\n";
						continue;
					}
					$name = strtoupper(trim($line[$iname]));
					$name = $this->remove_nonascii($name);
					$nrid = strtoupper(trim($line[$inrid]));
					$nrid = $this->remove_nonascii($nrid);
					if ((strlen($nrid)<STUDENT_NRIC_SIZE||
							!$this->validate_nrid($nrid))&&($chk===$unid)) {
						$emsg = $emsg."** Line ".$step.": ";
						$emsg = $emsg."Invalid student nrid! (".$nrid.")<br>\n";
						continue;
					}
					$prog = strtoupper(preg_replace('/\s/','',$line[$iprog]));
					$prog = $this->remove_nonascii($prog);
					$pchk = $this->_dodata->findProg($prog);
					$prog = $pchk['id']; //$pchk['name'] available
					$lgrp = strtoupper(trim($line[$ilgrp]));
					$mgrp = strtoupper(trim($line[$imgrp]));
					if ($iflag>0) {
						$flag = intval($line[$iflag]);
						switch ($flag) {
							case STUDENT_FLAG_ACTIVE:
							case STUDENT_FLAG_DROP:
							case STUDENT_FLAG_OPTOUT:
								break;
							default: $flag = 0;
						}
					}
					else $flag = STUDENT_FLAG_ACTIVE;
					$stud = $this->_dodata->findStudent($unid);
					if ($stud['stat']==true) {
						if ($stud['name']!=$name||$stud['bkid']!=$nrid) {
							$emsg = $emsg."** Line ".$step.": ";
							$emsg = $emsg."Duplicate student ".
								json_encode($stud)."(CSV:".$name.",".
								$nrid.")"."'!<br>\n";
							continue;
						}
						if ($prog!=0&&$prog!=$stud['flag']) {
							$this->_dodata->modifyStudent($unid,$prog);
							$imsg = $imsg."## Line ".$step.": ";
							$imsg = $imsg."Modify student '".
								$unid."' prog to '".$prog."!<br>\n";
						}
					}
					else {
						$this->_dodata->createStudent($unid,$name,$nrid,$prog);
						$stud = $this->_dodata->findStudent($unid);
						if ($stud['stat']==false) {
							$emsg = $emsg."** Line ".$step.": ";
							$emsg = $emsg."Failed to create student '".
								$unid."'@'".$name."'!<br>\n";
							continue;
						}
						else {
							$imsg = $imsg."## Line ".$step.": ";
							$imsg = $imsg."Student user '".
								$unid."'@'".$name."' created!<br>\n";
						}
					}
					$stid = $stud['id'];
					$find = $this->_dodata->findCourseStudent($table,$stid);
					if ($find['stat']==false) {
						$this->_dodata->createCourseStudent($table,
							$stid,$lgrp,$mgrp,$flag);
						$find = $this->_dodata->findCourseStudent($table,$stid);
						if ($find['stat']==false) {
							$emsg = $emsg."** Line ".$step.": ";
							$emsg = $emsg."Failed to add student '".
								$unid."'!<br>\n";
						}
						else {
							$imsg = $imsg."## Line ".$step.": ";
							$imsg = $imsg."Student '".$unid."' added to '".
								$table."'!<br>\n";
						}
					} // ignore if already in course
				}
				break;
			}
			case TASK_STAFF_UPDATE_MARKS: {
				// find name and unid columns
				$iname = -1; $iunid = -1;
				$idata = -1; $count = 0;
				$find = null;
				$core = $this->_dodata->findCourse($_POST["codeId"]);
				$this->_dodata->selectSession($_POST["ssemId"]);
				$cors = $this->_dodata->listCoursesComponents($core['id']);
				foreach ($csvd['headline'] as $head) {
					$temp = strtoupper($head);
					if ($temp==HEADER_STUDENT_NAME) $iname = $count;
					else if ($temp==HEADER_STUDENT_UNID) $iunid = $count;
					else if ($idata<0) {
						foreach ($cors['list'] as $item) {
							if ($temp===$item['name']) {
								$idata = $count;
								$find = $item['name'];
								break;
							}
						}
					}
					$count++;
				}
				if ($iname<0||$iunid<0||$idata<0) {
					$this->throw_debug('Invalid format?!('.
						$iname.','.$iunid.','.$idata.')');
				}
				$table = $_POST["codeId"]."_".$_POST["ssemId"];
				$this->_dodata->checkStudents();
				$this->_dodata->checkCourseStudent($table,$core['id']);
				foreach ($csvd['dataline'] as $line) {
					if (empty($line[$iname])||empty($line[$iunid])||
							empty($line[$idata])) {
						//$this->throw_debug('Empty fields!=>'.json_encode($line));
						continue;
					}
					$name = strtoupper(trim($line[$iname]));
					$unid = trim($line[$iunid]);
					$mark = floatval(trim($line[$idata]));
					$stud = $this->_dodata->findStudent($unid);
					if ($stud['stat']==true) {
						if ($stud['name']!=$name) {
							$this->throw_debug('Mistaken identity!');
						}
						$tdata = $idata;
						$next = $find;
						while ($tdata<$count) {
							$mark = floatval(trim($line[$tdata]));
							// update!
							$this->_dodata->updateCourseStudentMark($table,
								$stud['id'],$next,$mark);
							$tdata++;
							if ($tdata==$count) break;
							$next = null;
							$temp = strtoupper($csvd['headline'][$tdata]);
							foreach ($cors['list'] as $item) {
								if ($temp===$item['name']) {
									$next = $item['name'];
									break;
								}
							}
							if ($next===null) break;
						}
					}
					else $this->throw_debug("Student ".$name." NOT found!");
				}
				break;
			}
			default: $this->throw_debug('Unknown error?!');
		}
		$text = "";
		if ($emsg!=="") {
			$text = "<br>\n".$emsg;
			$this->_dotext = "Data upload done with error!<br>";
		}
		if ($imsg!=="") $text = $text."<br>\n".$imsg;
		$this->_dotext = $this->_dotext.$text;
	}
	function validate_nrid($nrid) {
		$zone = new DateTimeZone(DEFAULT_TIMEZONE);
		$date = new DateTime("now",$zone);
		$year = $date->format('Y');
		$dec = intval(substr($year,0,2));
		$ttt = intval(substr($year,2,2));
		$yyy = intval(substr($nrid,0,2));
		if ($yyy>$ttt) $dec--;
		$yyy = $yyy + ($dec*100);
		$mmm = intval(substr($nrid,2,2));
		$ddd = intval(substr($nrid,4,2));
		if (checkdate($mmm,$ddd,$yyy)) {
			return true;
		}
		// give it another chance
		$yyy -= 100;
		return checkdate($mmm,$ddd,$yyy);
	}
	function remove_nonascii($text) {
		return preg_replace("/[^\x01-\x7f]/","",$text);
	}
}
?>
