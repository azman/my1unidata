<?php
require_once dirname(__FILE__).'/PageDone.php';
class PagePassRst extends PageDone {
	function __construct() {
		parent::__construct('Password Reset');
		if (!isset($_GET['unid']))
			$this->throw_debug("Invalid password request!");
		$item = $this->_dodata->resetPass($_GET['unid']);
		$this->_doskip = 1;
		if ($item===false)
			$this->_dotext = 'Password reset failed! Invalid user?';
		else
			$this->_dotext = 'Password reset for '.$item['name'].'.';
	}
	function build_page() {
		parent::build_page();
	}
}
?>
