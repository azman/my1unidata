<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageStudentEnableCh extends PageDone {
	use TraitUniData;
	function __construct() {
		parent::__construct("Student Status Update");
		$stid = $_GET['stid'];
		$flag = $this->_dodata->isFlagUser($stid);
		switch ($_GET['do']) {
			case "disableStud":
				$flag |= FLAG_INACTIVE;
				$mesg = "Student now inactive.";
				break;
			case "enableStud": default:
				$flag &= ~FLAG_INACTIVE;
				$mesg = "Student now active.";
				break;
		}
		$this->_dodata->doFlagUser($stid,$flag);
		$this->_dotext = $mesg;
		$this->_doskip = 1;
	}
}
?>
