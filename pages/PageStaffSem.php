<?php
require_once dirname(__FILE__).'/PageData.php';
class PageStaffSem extends PageData {
	protected $_code;
	protected $_ssem;
	function __construct() {
		// get params
		$code = $_GET['code'];
		$ssem = intval($_GET['ssem']);
		parent::__construct('Assign Staff for '.$code.' - '.$ssem);
		$this->_code = $code;
		$this->_ssem = $ssem;
	}
	function jsobj_main() {
		$js_main = <<< JS_MAIN
function post_check() {
	var chk_form = document.getElementById('form_course');
	chk_form.cCode.disabled = false;
	return true;
}
JS_MAIN;
		$jsobj = new JSObject('js_main');
		$jsobj->insert_inner($js_main);
		return $jsobj;
	}
	function build_page() {
		$code = $this->_code;
		$ssem = $this->_ssem;
		$user = $this->_dodata->getProfile();
		$item = $this->_dodata->findCourse($code);
		if ($item['stat']==false) {
			$this->throw_debug('Invalid course code?!');
		}
		$view = $this->_doview;
		$view->insert_page_title();
		$temp = $this->jsobj_main();
		$this->insert_2body($temp);
		$menu = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$view->insert_menu($menu);
		// create form
		$form = $view->create_form('form_semcourse');
		$form->insert_onsubmit('javascript:return post_check();');
		$view->insert_form($form);
		$view->create_form_input_hidden($form,'code',null,$code);
		$view->create_form_input_hidden($form,'ssem',null,$ssem);
		$view->create_form_input_text($form,'Course Code','cCode',
			[ "tval"=>$code , "ro"=>1 , "linebr"=>"1" ]);
		$view->create_form_input_text($form,'Course Name','cName',
			[ "tval"=>$item['name'] , "ro"=>1 , "linebr"=>"1" ]);
		// selection for staff
		$temp = $view->create_form_select($form,
			"Select Staff (Ctrl+Click to select multiple)",
			"pickStaff[]",[],["linebr"=>2]);
		$temp->insert_keyvalue("size","10",true);
		// create options here
		$size = 0;
		$staf = $this->_dodata->listStaffs();
		foreach ($staf['list'] as $item) {
			if ($item['unid']===DEFAULT_ROOT_UNID) continue;
			$unid = $item['unid'];
			$test = null;
			$slst = $this->_dodata->listCoursesStaffs($unid,$code,$ssem);
			if ($slst['stat']===true) {
				$test = $slst['list'][0];
				$test['role'] = intval($test['role']);
			}
			if ($test!==null&&$test['role']===COURSE_ADMIN) continue;
			$chkd = ($test!==null&&$test['role']===COURSE_STAFF)?true:false;
			$view->create_form_select_option($form,$temp,
				$item['name'],$item['unid'],$chkd);
			$size++;
		}
		$temp->insert_constant('multiple');
		$temp->insert_keyvalue('size',$size);
		$temp->insert_keyvalue('style','overflow-y:auto');
		$view->create_form_submit($form,'Submit','postSelectStaffSem');
		$view->insert_menu($menu);
	}
}
?>
