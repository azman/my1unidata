<?php
require_once dirname(__FILE__).'/../include/TraitUniData.php';
require_once dirname(__FILE__).'/PageDone.php';
class PageStaffSemRemove extends PageDone {
	use TraitUniData;
	function __construct() {
		$coid = intval($_GET['coid']);
		$stid = intval($_GET['stid']);
		$ssem = intval($_GET['ssem']);
		parent::__construct("Staff removed from course @".$ssem);
		$this->_dodata->createCoursesStaffs($coid,$stid,$ssem,COURSE_CHECK);
		$this->_dotext = "Semester staff assignment updated!";
		$this->_doskip = 1;
	}
}
?>
