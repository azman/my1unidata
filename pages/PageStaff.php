<?php
require_once dirname(__FILE__).'/PageData.php';
class PageStaff extends PageData {
	function __construct() {
		$this->_title_ = 'Staff List';
		parent::__construct();
	}
	function build_page() {
		$user = $this->_dodata->getProfile();
		$staf = $this->_dodata->listStaffs();
		$view = $this->_doview;
		$view->insert_page_title();
		// create return link & csv download link
		$menu = $view->menu_list_item_linkback(null,SINGLE_BACK);
		$menu = $view->menu_list_item_command($menu,
			"viewstaff&fmt=csv","Download CSV");
		$view->insert_menu($menu);
		if ($user['type']===USER_ADMIN) {
			// create form to upload more staff info
			$form = $view->create_form('form_upstaf');
			$form->insert_keyvalue('enctype','multipart/form-data');
			$view->create_form_input_hidden($form,'unid',null,$user['unid']);
			$view->create_form_input_hidden($form,'aCmd',
				null,TASK_STAFF_CREATE_STAFF);
			$view->create_form_input_file($form,'Data File (CSV)','dataFile',
				["linebr"=>1]);
			$view->create_form_submit($form,
				'Upload Staff Info','postUploadStaff');
			$view->insert_form($form,[ "class" =>
				"w3-margin-left w3-margin-top" ]);
		}
		// create table for list of staffs (minus admin)
		$size = count($staf['list'])-1;
		if ($size>0) {
			$view->insert_highlight("<i>Staff Count</i>: ".$size);
			$ttab = $view->create_table();
			$view->insert_table($ttab);
			$view->create_table_header_row($ttab);
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STAFF_UNID);
			if ($user['type']===USER_ADMIN) {
				$tcol = $view->create_table_header_col($ttab);
				$tcol->insert_inner(HEADER_STAFF_NRIC);
			}
			$tcol = $view->create_table_header_col($ttab);
			$tcol->insert_inner(HEADER_STAFF_NAME);
			if ($user['type']===USER_ADMIN) {
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner(HEADER_STAFF_NICK);
				$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner("USER LEVEL");
			}
			$tcol = $view->create_table_header_col($ttab,
					[ "class"=>"w3-center" ]);
			$tcol->insert_inner("Assigned Course(s)");
			foreach ($staf['list'] as $item) {
				if ($item['unid']===DEFAULT_ROOT_UNID) continue;
				$view->create_table_data_row($ttab);
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['unid']."&nbsp;&nbsp;");
				if ($user['type']===USER_ADMIN) {
					$show = $view->create_menu_item("rstpass&unid=".
						$item['unid'],"Reset Password",
						MENUITEM_CMD_,[ "color" => "w3-red w3-small" ]);
					$tcol->append_object($show);
				}
				if ($user['type']===USER_ADMIN) {
					$tcol = $view->create_table_data_col($ttab);
					$tcol->insert_inner($item['bkid']);
				}
				$tcol = $view->create_table_data_col($ttab);
				$tcol->insert_inner($item['name']);
				if ($user['type']===USER_ADMIN) {
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($item['nick']);
					$item['type'] = intval($item['type']);
					switch ($item['type']) {
						case USER_ADMIN: $text = "Administrator"; break;
						case USER_STAFF: $text = "Staff"; break;
						default: $text = "Unknown";
					}
					$tcol = $view->create_table_data_col($ttab,
						[ "class"=>"w3-center" ]);
					$tcol->insert_inner($text);
				}
				// course assigned
				$this->_dodata->checkCourseStaff();
				$cors = $this->_dodata->listCourseStaff(null,$item['unid']);
				$text = "";
				if ($cors['stat']===true) {
					foreach ($cors['list'] as $that) {
						$that['role'] = intval($that['role']);
						if ($that['role']===COURSE_CHECK) continue;
						if ($text!=="") $text = $text.", ";
						$text = $text.$that['course'];
						if ($that['role']===COURSE_ADMIN) {
							$text = $text.'(C)';
						}
					}
				}
				$tcol = $view->create_table_data_col($ttab,
					[ "class"=>"w3-center" ]);
				$tcol->insert_inner($text);
			}
		}
		else $view->insert_highlight('No staff profile found in database.');
		$view->insert_menu($menu);
	}
	function sendCSV() {
		$head =  [ HEADER_STAFF_UNID,
			HEADER_STAFF_NRIC, HEADER_STAFF_NAME, HEADER_STAFF_NICK ];
		$data = [];
		$staf = $this->_dodata->listStaffs();
		if ($staf['stat']===true) {
			foreach ($staf['list'] as $item) {
				if ($item['unid']===DEFAULT_ROOT_UNID) continue;
				array_push($data,[$item['unid'],$item['bkid']
					,$item['name'],$item['nick']]);
			}
		}
		require_once dirname(__FILE__).'/../include/FileText.php';
		$fcsv = new FileText();
		$fcsv->sendCSV('liststaff.csv',$head,$data);
	}
}
?>
