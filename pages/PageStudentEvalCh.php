<?php
require_once dirname(__FILE__).'/PageDone.php';
class PageStudentEvalCh extends PageDone {
	function __construct() {
		if (empty($_POST['code'])||empty($_POST['ssem'])||
				empty($_POST['coid'])||empty($_POST['stid'])||
				empty($_POST['unid'])||empty($_POST['postChStudentEval'])) {
			$this->throw_debug('Invalid evaluation info?!'.json_encode($_POST));
		}
		// check partA
		for ($loop=1;$loop<=9;$loop++) {
			if (empty($_POST['A'.$loop])) {
				$this->throw_debug('Invalid evaluation info?!');
			}
		}
		// check partB
		for ($loop=1;$loop<=7;$loop++) {
			if (empty($_POST['B'.$loop])) {
				$this->throw_debug('Invalid evaluation info?!');
			}
		}
		// check partC
		for ($loop=1;$loop<=8;$loop++) {
			if (empty($_POST['C'.$loop])) {
				$this->throw_debug('Invalid evaluation info?!');
			}
		}
		// prepare timezone
		$tzone = new DateTimeZone(DEFAULT_TIMEZONE);
		//$tzutc = new DateTimeZone("UTC");
		// create datetime object
		$tdate = new DateTime("now",$tzone);
		// prepare db data
		$mark = $tdate->format('YmdHis');
		$code = $_POST['code'];
		$unid = $_POST['unid'];
		$ssem = intval($_POST['ssem']);
		$stid = intval($_POST['stid']);
		$coid = intval($_POST['coid']);
		$lecd = intval($_POST['pickStaff']);
		$labd = intval($_POST['pickLabIn']);
		$comm = $_POST['comment'];
		$tabs = $code.'_'.$ssem;
		$eval = $tabs.'_EVAL';
		$stat = $tabs.'_EVALSTAT';
		parent::__construct();
		// validate data?
		$test = $this->_dodata->findCourse($code);
		if ($test['stat']!==true||$test['id']!==$coid) {
			$this->throw_debug('Invalid course code!');
		}
		$test = $this->_dodata->findCourseStudent($tabs,$stid);
		if ($test['stat']!==true) {
			$this->throw_debug('Invalid student id!');
		}
		// pack questions
		$paqx = [];
		for ($loop=1;$loop<=9;$loop++) {
			$paqx["A".$loop] = $_POST["A".$loop];
		}
		$pbqx = [];
		for ($loop=1;$loop<=7;$loop++) {
			$pbqx["B".$loop] = $_POST["B".$loop];
		}
		$pcqx = [];
		for ($loop=1;$loop<=8;$loop++) {
			$pcqx["C".$loop] = $_POST["C".$loop];
		}
		// insert evaluation
		$this->_dodata->createCourseEval($eval,$mark,$lecd,$labd,
			$paqx,$pbqx,$pcqx,$comm);
		$item = $this->_dodata->getStaffName($lecd);
		$text = "Lecturer: ".$item['name'];
		$item = $this->_dodata->getStaffName($labd);
		$text = $text."<br>\nLab Instructor: ".$item['name'];
		$text = $text."<br>\nTimestamp: $mark";
		$text = "Course evaluation for $code commited!<br><br>".$text;
		$this->done_message($text);
		$this->done_header("$code Evaluation Completed");
	}
}
?>
