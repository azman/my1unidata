<?php
require_once dirname(__FILE__).'/../include/TraitBase.php';
require_once dirname(__FILE__).'/Page.php';
define('DEFAULT_DONE_MESSAGE','Done.');
class PageDone extends Page {
	use TraitBase;
	protected $_doskip;
	protected $_dotext;
	protected $_dohead;
	function __construct($page_title=null,$base=true) {
		if ($page_title!==null)
			$this->_title_ = $page_title;
		parent::__construct();
		if ($base===true)
			$this->initialize_base(LOGIN_MODE,DEFAULT_DATA_CLASS);
		$this->_doskip = DEFAULT_BACK_COUNT;
		$this->_dotext = DEFAULT_DONE_MESSAGE;
		$this->_dohead = "";
	}
	function Data() {
		return $this->_dodata;
	}
	function back_count($count=DEFAULT_BACK_COUNT) {
		$count = intval($count);
		if ($count<0) $count = -$count;
		$this->_doskip = $count;
	}
	function done_message($message=DEFAULT_DONE_MESSAGE) {
		$this->_dotext = $message;
	}
	function done_header($header="") {
		$this->_dohead = $header;
	}
	function build_page() {
		$view = $this->_doview;
		if ($this->_title_!="")
			$view->insert_page_title();
		if ($this->_dohead!="")
			$view->insert_page_section($this->_dohead);
		$view->insert_highlight($this->_dotext);
		$list = $view->menu_list_item_linkback(null,$this->_doskip);
		$view->insert_menu($list);
	}
}
?>
