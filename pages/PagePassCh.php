<?php
require_once dirname(__FILE__).'/PageDone.php';
class PagePassCh extends PageDone {
	function __construct() {
		$this->_title_ = 'Change Password';
		parent::__construct();
		// modify pass
		$this->_dodata->modifyPass($_POST['user'],
			$_POST['pass'],$_POST['pasX']);
		// must be in session
		require_once dirname(__FILE__).'/../include/Session.php';
		$session = new Session();
		if ($session->Validate()!==true) {
			$this->throw_debug('Invalid Session!');
		}
		// update session?
		$_SESSION[SESSION_PASS] = $_POST['pasX'];
		// prepare page
		$user = $this->_dodata->getProfile();
		$this->_dotext = 'Password changed for '.$user['name'].'.';
		$this->_doskip = DOUBLE_BACK;
	}
}
?>
