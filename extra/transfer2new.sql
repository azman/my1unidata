ALTER TABLE users RENAME TO users_old ;
CREATE TABLE users ( id INTEGER PRIMARY KEY, unid TEXT UNIQUE NOT NULL, bkid TEXT NOT NULL, pass TEXT NOT NULL, name TEXT NOT NULL, nick TEXT NOT NULL, type INTEGER, flag INTEGER, UNIQUE (unid,bkid)) ;
INSERT INTO users (id,unid,bkid,pass,name,nick,type,flag) SELECT id,unid,bkid,pass,name,nick,type,flag from users_old ;

DROP VIEW courses_staffs_view ;
CREATE VIEW courses_staffs_view AS SELECT T1.ssem AS ssem, T2.code AS course, T2.name AS coursename, T3.unid AS staff, T3.name AS staffname, T3.nick AS staffnick, T1.coid AS coid, T1.stid AS stid, T1.flag AS role FROM courses_staffs T1, courses T2, users T3 WHERE T1.coid=T2.id AND T1.stid=T3.id;
DROP VIEW course_staff_view ;
CREATE VIEW course_staff_view AS SELECT T2.code AS course, T2.name AS coursename, T3.unid AS staff, T3.nick AS staffname, T1.coid AS coid, T1.stid AS stid, T1.alvl AS role FROM course_staff T1, courses T2, users T3 WHERE T1.coid=T2.id AND T1.stid=T3.id;
