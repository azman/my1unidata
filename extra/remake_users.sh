#!/bin/bash

THAT=`dirname $0`
THAT=`cd $THAT;pwd`

SQLITE_EXEC="sqlite3"
SQLITE_FILE="unidata.sqlite"
SQLITE_CMDS="${THAT}/transfer2new.sql"

[ ! -f "$SQLITE_CMDS" ] && echo "Cannot find $SQLITE_CMDS!" && exit 1

[ ! -f "$SQLITE_FILE" ] && echo "Cannot find $SQLITE_FILE!" && exit 1

SQLITE_TEST=`which $SQLITE_EXEC 2>/dev/null`
[ "$SQLITE_TEST" = "" ] && echo "Cannot exec $SQLITE_EXEC!" && exit 1

# make backup
cp ${SQLITE_FILE} ${SQLITE_FILE}.bak
# run transfer commands
$SQLITE_TEST $SQLITE_FILE <$SQLITE_CMDS
# check if transfer valid
$SQLITE_TEST $SQLITE_FILE 'select * from users_old' > chkold.txt
$SQLITE_TEST $SQLITE_FILE 'select * from users' > chknew.txt
diff chknew.txt chkold.txt 2>/dev/null
if [ $? -ne 0 ] ; then
	echo "Invalid transfer?"
else
	$SQLITE_TEST $SQLITE_FILE 'drop table users_old'
	echo "Transfer OK!"
fi
rm -f chknew.txt chkold.txt
