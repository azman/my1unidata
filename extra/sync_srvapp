#!/bin/bash

# run this in child repo to sync with my1srvapp
TOOL_NAME=$(basename $0)
TOOL_PATH=$(cd $(dirname $0);pwd)
THIS_PATH=$(dirname $TOOL_PATH)

MAIN_PATH=$1
[ ! -d "$MAIN_PATH" ] &&
	echo "** Need path to my1srvapp repository!" && exit 1
MAIN_PATH=$(cd $MAIN_PATH;pwd)
# must have this script
[ ! -d $MAIN_PATH/extra -o ! -f $MAIN_PATH/extra/$TOOL_NAME ] &&
	echo "** NOT my1srvapp repository? ($MAIN_PATH)" && exit 1

SYNC_OPTX=$2
[ ! -z "$SYNC_OPTX" -a "$SYNC_OPTX" != "-x" ] &&
	echo "** Invalid option '$SYNC_OPTX'!" && exit 1

# find all files... not in data
FIND_OPTS="-type f -not -path $MAIN_PATH/data/*"
# also skip local config
FIND_OPTS="$FIND_OPTS -not -name *.local"
# ignore hidden files... assume those are unique to each
SYNC_LIST=$(find $MAIN_PATH/ \( ! -regex '.*/\..*' \) $FIND_OPTS)
# should be at least one...
DIF_FOUND=0
for THAT in $SYNC_LIST ; do
	TREL=$(realpath $THAT --relative-to=$MAIN_PATH)
	LOOK=$THIS_PATH/$TREL
	if [ ! -f $LOOK ] ; then
		echo "** Missing:$TREL"
		if [ ! -z "$SYNC_OPTX" ] ;  then
			CHKP=$THIS_PATH/$(dirname $TREL)
			if [ ! -d "$CHKP" ] ; then
				echo -n "## Creating path '$CHKP'... "
				mkdir -p $CHKP
				[ $? -ne 0 ] && echo "**error**" && continue
				echo "done."
			fi
			echo -n "## Copying '$TREL'... "
			cp $THAT $CHKP/
			[ $? -ne 0 ] && echo "**error**" && continue
			echo "done."
		fi
		DIF_FOUND=1
	else
		DIFF=$(diff $THAT $LOOK)
		if [ ! -z "$DIFF" ] ; then
			DIF_FOUND=1
			echo "@@ Diff:$TREL"
			echo "@@ [INITDIFF]"
			echo "$DIFF"
			echo "@@ [DONEDIFF]"
			if [ $THAT -nt $LOOK ] ; then
				echo ">> SRC is newer!"
				if [ ! -z "$SYNC_OPTX" ] ;  then
					echo -n "## Copying FROM source '$TREL'... "
					cp $THAT $LOOK
					[ $? -ne 0 ] && echo "**error**" && continue
					echo "done."
				fi
			else
				echo ">> DST is newer!"
				if [ ! -z "$SYNC_OPTX" ] ;  then
					echo -n "## Copying TO source '$TREL'... "
					cp $LOOK $THAT
					[ $? -ne 0 ] && echo "**error**" && continue
					echo "done."
				fi
			fi
		fi
	fi
done
[ $DIF_FOUND -eq 0 ] &&
	echo "## All in $(basename $THIS_PATH) sync to $(basename $MAIN_PATH)!"
