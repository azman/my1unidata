<?php
trait TraitUniData {
	// MUST BE used along with TraitBase
	function data_upgrade() {
		$user = $this->_dodata->getProfile();
		if ($user['type']==USER_STAFF||$user['type']==USER_ADMIN) {
			$this->_dbdata = ADMIN_DATA_CLASS;
			$this->initialize_data();
		}
	}
}
?>
