<?php
trait TraitNavbar  {
	function jsobj_navbar() {
		$temp = new JSObject('js_navbar');
		$that = <<<JSCODE_NAVBAR
var my1side = null;
var my1olay = null;
function find_side() { // call this on document ready
	my1side = document.getElementById("side_main");
	my1olay = document.getElementById("side_olay");
}
function my1side_open() {
	if (my1side.style.display === 'block') {
		my1side.style.display = "none";
		my1olay.style.display = "none";
	} else {
		my1side.style.display = "block";
		my1olay.style.display = "block";
	}
}
function my1side_done() {
	my1side.style.display = "none";
	my1olay.style.display = "none";
}
JSCODE_NAVBAR;
		$temp->insert_inner($that);
		return $temp;
	}
	function insert_top_bar($title) {
		$main = new HTMLObject('div');
		$main->insert_class("w3-bar w3-top w3-theme-d2 w3-large");
		$main->insert_style("z-index:4;");
		$main->do_multiline();
		$this->append_2body($main);
		// button
		$butt = new HTMLObject('button');
		$pick = "w3-bar-item w3-button w3-hide-large w3-hover-none";
		$pick = $pick." w3-hover-text-light-grey";
		$butt->insert_class($pick);
		$butt->insert_keyvalue("onclick","my1side_open();");
		$butt->do_1skipline();
		$main->append_object($butt);
		// icon
		$temp = new HTMLObject('i');
		$temp->insert_class("fa-solid fa-bars");
		$temp->do_1skipline();
		$butt->append_object($temp);
		// text
		$temp = new HTMLObject('span');
		// w3-middle not working here? cannot see in firefox inspector
		$temp->insert_class("w3-bar-item w3-right w3-middle");
		$temp->insert_inner($title);
		$temp->do_1skipline();
		$main->append_object($temp);
		return $main;
	}
	function insert_side_nav_item($side,$text,$icon,$href=null) {
		$that = new HTMLObject('a');
		if ($href!==null)
			$that->insert_keyvalue("href",$href);
		$what = "w3-bar-item w3-button";
		if ($icon==="fa-remove") { // special class
			$what = $what." w3-padding-16";
			$what = $what." w3-hide-large w3-dark-grey w3-hover-black";
		} else $what = $what." w3-padding";
		$that->insert_class($what);
		$that->insert_inner("  ".$text,false); // text AFTER icon
		$that->do_1skipline();
		$side->append_object($that);
		// icon
		$temp = new HTMLObject('i');
		$temp->insert_class("fa-solid ".$icon." fa-fw");
		$that->append_object($temp);
		return $that;
	}
	function insert_side_nav() {
		$olay = new HTMLObject('div');
		$olay->insert_id("side_olay");
		$olay->insert_class("w3-overlay w3-hide-large w3-animate-opacity");
		$olay->insert_style("cursor:pointer;display:none;");
		$olay->insert_keyvalue("onclick","my1side_done();");
		$olay->insert_keyvalue("title","Close Side Menu");
		$olay->do_1skipline();
		$this->append_2body($olay);
		$main = new HTMLObject('nav');
		$main->insert_id('side_main');
		$main->insert_class("w3-sidebar w3-collapse w3-white w3-animate-left");
		$pick = "z-index:3;width:300px;display:none;padding-top:4em;";
		$main->insert_style($pick);
		$main->do_multiline();
		$this->append_2body($main);
		// menu block
		$temp = new HTMLObject('div');
		$temp->insert_class("w3-bar-block");
		$temp->do_multiline();
		$main->append_object($temp);
		$this->_domenu = $temp;
		// menu items
		$that = $this->insert_side_nav_item($temp,
			"Close Menu","fa-remove",null);
		$that->insert_keyvalue("onclick","my1side_done();");
		$that->insert_keyvalue('title',"Close Menu");
		//$this->insert_side_nav_item($temp,"Users","fa-users",null);
		//$this->insert_side_nav_item($temp,"Things","fa-share-alt",null);
		return $temp; // div menu to insert more items
	}
}
?>
