<?php
trait TraitUpload {
	// trait to upload csv-formatted data
	protected $_csvd;
	function upload() {
		// check user
		if (property_exists($this,'_dodata')) {
			$user = $this->_dodata->getProfile();
			if (empty($_POST["unid"]))
				$this->throw_debug("No Staff ID!");
			if ($_POST["unid"]!=$user["unid"])
				$this->throw_debug("Invalid ID!");
		}
		// check upload error... from php.net
		if (!isset($_FILES['dataFile']['error'])||
				is_array($_FILES['dataFile']['error'])) {
			$this->throw_debug('Invalid parameters!');
		}
		switch ($_FILES['dataFile']['error']) {
			case UPLOAD_ERR_OK:
				break;
			case UPLOAD_ERR_NO_FILE:
				$this->throw_debug('No file sent.');
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				$this->throw_debug('Exceeded filesize limit.');
			default:
				$this->throw_debug('Unknown errors.');
		}
		// enforce these!
		if ($_FILES["dataFile"]["size"]==0)
			$this->throw_debug('Empty file?!');
		if ($_FILES["dataFile"]["size"]>500000)
			$this->throw_debug('File too large!');
		// simply load temporary file
		$filename = $_FILES["dataFile"]["tmp_name"];
		require_once dirname(__FILE__).'/../include/FileText.php';
		$file = new FileText();
		$this->_csvd = $file->loadCSV($filename);
		if ($this->_csvd['stat']!==true)
			$this->throw_debug('Cannot load data!');
		else if ($this->_csvd['rows']==0)
			$this->throw_debug('No data found!');
	}
}
?>
