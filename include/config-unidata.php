<?php
define('MY1CFGUNIDATA',(__FILE__).'.local');
if (file_exists(MY1CFGUNIDATA)) require_once MY1CFGUNIDATA;
define_if_not_exists('MY1CFGINIT',MY1CFGUNIDATA);
// application general contants
define_if_not_exists('DEBUG_MODE',false);
define_if_not_exists('MY1APP_TITLE','MY1 UniData');
define_if_not_exists('SESSION_PREFIX','my1unidata');
// installation stuff
define_if_not_exists('DEFAULT_ROOT_UNID','0100000');
define_if_not_exists('DEFAULT_ROOT_BKID','010101000101');
define_if_not_exists('DEFAULT_ROOT_NAME','ROOT');
// Data constants
define_if_not_exists('DEFAULT_DATA_CLASS','DataStudent');
define_if_not_exists('ADMIN_DATA_CLASS','DataStaff');
define_if_not_exists('DATA_FILE','unidata.sqlite');
// default pages
define_if_not_exists('DEFAULT_THEME','ThemeNone');
define_if_not_exists('PAGE_MAIN','PageCheck');
// task handlers
define_if_not_exists('TASK_GET','TaskGETuni');
define_if_not_exists('TASK_POST','TaskPOSTuni');
// student status
define('STUDENT_FLAG_ACTIVE',1);
define('STUDENT_FLAG_DROP',3);
define('STUDENT_FLAG_OPTOUT',5);
define('STUDENT_FLAG_MASK',15); // 0x0f
// commands
define('TASK_STAFF_CREATE_STAFF',1);
define('TASK_STAFF_CREATE_COURSE',2);
define('TASK_STAFF_EXECUTE_COURSE',3);
define('TASK_STAFF_ADD_STUDENTS',4);
define('TASK_STAFF_UPDATE_MARKS',5);
// data headers
define('HEADER_STAFF_UNID','STAFFID');
define('HEADER_STAFF_NRIC','NRIC');
define('HEADER_STAFF_NAME','NAME');
define('HEADER_STAFF_NICK','NICK');
define('HEADER_COURSE_CODE','CODE');
define('HEADER_COURSE_NAME','NAME');
define('HEADER_COURSE_UNIT','UNIT');
define('HEADER_COURSE_ASEM','ASEM');
define('HEADER_CCOMP_NAME','NAME');
define('HEADER_CCOMP_RAW_','RAWMARK');
define('HEADER_CCOMP_PCT_','PERCENTAGE');
define('HEADER_CCOMP_LABEL','LABEL');
define('HEADER_CCOMP_GROUP','GROUP');
define('HEADER_CCOMP_SUBGRP','SUBGROUP');
define('HEADER_CCOMP_INDEX','INDEX');
define('HEADER_STUDENT_UNID','STUDENTID');
define('HEADER_STUDENT_NAME','NAME');
define('HEADER_STUDENT_NRIC','NRIC');
define('HEADER_STUDENT_PROG','PROGRAM');
define('HEADER_STUDENT_LGRP','LGROUP');
define('HEADER_STUDENT_MGRP','MGROUP');
define('HEADER_STUDENT_FLAG','FLAG');
// COURSE SEMESTER
define('COURSE_SEMESTER1',0x01);
define('COURSE_SEMESTER2',0x02);
// course staff level
define('COURSE_CHECK',0);
define('COURSE_ADMIN',1);
define('COURSE_STAFF',2);
// sizes
define('STAFF_UNID_SIZE',7);
define('STAFF_NRIC_SIZE',12);
define('STUDENT_UNID_SIZE',9);
define('STUDENT_NRIC_SIZE',12);
// system timezone
define('DEFAULT_TIMEZONE',"Asia/Kuala_Lumpur");
?>
