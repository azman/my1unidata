<?php
/**
 * Base.php
 * - base class for database interface
**/
require_once dirname(__FILE__).'/Thing.php';
class Base extends Thing {
	protected $_dbname;
	protected $_dbuser;
	protected $_dbpass;
	protected $_dblite;
	protected $_dbmain; // database object
	function __construct($lite=DEFAULT_DBPATH,$name=DATA_FILE) {
		parent::__construct();
		if ($lite===null) {
			// using mariadb
			$this->_dblite = null;
			$this->_dbname = MARIADB_NAME;
			$this->_dbuser = MARIADB_USER;
			$this->_dbpass = MARIADB_PASS;
		}
		else {
			// using sqlite ($lite is path to sqlite file)
			$this->_dblite = $lite."/".$name;
			$this->_dbname = null;
			$this->_dbuser = null;
			$this->_dbpass = null;
		}
		$this->_dbmain = null;
	}
	function make($root="root",$pass="") {
		if ($this->_dblite===null) {
			try {
				$db = new PDO("mysql:",$root,$pass);
				$do = "CREATE DATABASE IF NOT EXISTS ".$this->_dbname;
				$db->exec($do);
				$do = "GRANT ALL privileges ON ".$this->_dbname.".*";
				$do = $do." TO '".$this->_dbuser."'@'localhost'";
				$do = $do." IDENTIFIED BY '".$this->_dbpass."';";
				// NOT NEEDED?
				//$do = $do." SET password FOR '".$this->_dbname."'@'%'";
				//$do = $do." = password('".$this->_dbpass."');";
				$do = $do." FLUSH privileges;";
				$db->exec($do);
			}
			catch ( PDOException $err ) {
				$this->throw_this("DB Failure! (".$err->getMessage().")");
			}
		}
		else {
			// create empty file for sqlite
			$dbpath = dirname($this->_dblite);
			if (!file_exists($dbpath)) {
				$uppath = realpath(dirname($dbpath));
				if (!is_dir($uppath))
					$this->throw_this("Missing parent path! (".$uppath.")");
				if (!is_writable($uppath))
					$this->throw_this("Cannot mod parent path! (".$uppath.")");
				if (mkdir($dbpath)===false)
					$this->throw_this("Cannot create path! (".$dbpath.")");
			}
			else if (!is_dir($dbpath))
				$this->throw_this("NOT a path! (".$$dbpath.")");
			$buff = substr(sprintf('%o',fileperms($dbpath)),-4);
			if ($buff!=="0777") {
				chmod($dbpath,0777);
				touch($this->_dblite);
				chmod($this->_dblite,0666);
			}
		}
	}
	function init() {
		try {
			if ($this->_dblite==null) {
				$this->_dbmain = new PDO("mysql:dbname=".$this->_dbname,
					$this->_dbuser,$this->_dbpass);
			}
			else {
				if (!file_exists($this->_dblite)) {
					touch($this->_dblite);
					chmod($this->_dblite,0666);
				}
				$this->_dbmain = new PDO("sqlite:".$this->_dblite);
				$this->_dbmain->setAttribute(PDO::ATTR_PERSISTENT,true);
			}
			$this->_dbmain->setAttribute(PDO::ATTR_ERRMODE,
				PDO::ERRMODE_EXCEPTION);
		}
		catch ( PDOException $error ) {
			$this->throw_this("DB Init Failed! (".
				$error->getMessage().")");
		}
	}
	function prepare($query) {
		try { // returns PDOStatement or FALSE
			$result = $this->_dbmain->prepare($query);
		}
		catch ( PDOException $error ) {
			$this->throw_this("Cannot prepare query! (".
				$error->getMessage().")");
		}
		return $result;
	}
	function query($query) {
		try { // returns PDOStatement or FALSE
			$result = $this->_dbmain->query($query);
		}
		catch ( PDOException $error ) {
			$this->throw_this("Cannot execute query! (".
				$error->getMessage().")");
		}
		return $result;
	}
	function table_exists($table) {
		// check if table exists
		if ($this->_dblite==null)
			$prep = "SHOW TABLES LIKE '".$table."'";
		else
			$prep = "SELECT name FROM sqlite_master WHERE type='table' ".
				"AND name='".$table."'";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_exists!");
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item==false) return false;
		return true;
	}
	function table_create($table,$tdata,$tmore=null) {
		// must exists
		if (empty($table)||empty($tdata)||!is_array($tdata)||
				(isset($tmore)&&!is_array($tmore))) {
			$this->throw_this("Not enough data for table_create!");
		}
		// create table
		$prep = "CREATE TABLE IF NOT EXISTS ".$table." ( ";
		foreach ($tdata as $cols) {
			if (!array_key_exists('name',$cols)||
					!array_key_exists('type',$cols))
				$this->throw_this("Invalid format for table_create!");
			$prep = $prep.$cols['name']." ".strtoupper($cols['type']).", ";
		}
		if (isset($tmore)) {
			foreach ($tmore as $xtra) {
				$prep = $prep.$xtra.", ";
			}
		}
		$prep = substr($prep,0,-2);
		$prep = $prep." )";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_create!");
		$stmt->closeCursor();
	}
	function table_delete($table) {
		// must exists
		if (empty($table))
			$this->throw_this("Not enough data for table_delete!");
		// delete table
		$prep = "DROP TABLE IF EXISTS ".$table;
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute table_delete!");
		$stmt->closeCursor();
	}
	function table_addcol($table,$tdata) {
		// must exists
		if (empty($table)||empty($tdata)||!is_array($tdata))
			$this->throw_this("Not enough data for table_addcol!");
		// WILL THIS WORK ON MARIADB?
		// alter table
		$init = "ALTER TABLE ".$table." ADD COLUMN ";
		foreach ($tdata as $cols) {
			if (!array_key_exists('name',$cols)||
					!array_key_exists('type',$cols))
				$this->throw_this('Invalid format for table_addcol!');
			$prep = $init.$cols['name']." ".strtoupper($cols['type']);
			$stmt = $this->prepare($prep);
			if (!$stmt->execute())
				$this->throw_this('Failed to execute table_addcol!');
			$stmt->closeCursor();
		}
	}
	function view_exists($view) {
		// check if view exists
		if ($this->_dblite==null)
			$prep = "SHOW TABLES LIKE '".$view."'";
		else
			$prep = "SELECT name FROM sqlite_master WHERE type='view' ".
				"AND name='".$view."'";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_exists!");
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item==false) return false;
		return true;
	}
	function view_create($view,$data,$more) {
		// must exists
		if (empty($view)||empty($data)||!is_array($data)||
				(isset($more)&&!is_array($more)))
			$this->throw_this("Not enough data for view_create!");
		// create view
		if ($this->_dblite==null)
			// temporary? IF NOT EXISTS not supported for mariadb < 10.1.3
			$prep = "CREATE VIEW ".$view." AS SELECT ";
		else
			$prep = "CREATE VIEW IF NOT EXISTS ".$view." AS SELECT ";
		foreach ($data as $cols) {
			if (!array_key_exists('which',$cols))
				$this->throw_this("Invalid format for view_create!");
			$prep = $prep.$cols['which'];
			if (array_key_exists('alias',$cols))
				$prep = $prep." AS ".$cols['alias'];
			$prep = $prep.", ";
		}
		$prep = substr($prep,0,-2);
		foreach ($more as $xtra) {
			$prep = $prep.$xtra;
		}
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_create!");
		$stmt->closeCursor();
	}
	function view_delete($view) {
		// must exists
		if (empty($view))
			$this->throw_this("Not enough data for view_delete!");
		// delete view
		if ($this->_dblite==null) $prep = "DROP VIEW ".$view;
		else $prep = "DROP VIEW IF EXISTS ".$view;
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_this("Failed to execute view_delete!");
		$stmt->closeCursor();
	}
}
?>
