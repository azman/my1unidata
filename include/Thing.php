<?php
/**
 * Thing.php
 * - base class for everyThing
**/
require_once dirname(__FILE__).'/config.php';
class Thing {
	protected $_name;
	function __construct() {
		$this->_name = get_class($this);
	}
	protected function throw_this($error) {
		throw new Exception("[$this->_name] ".$error);
	}
}
?>
