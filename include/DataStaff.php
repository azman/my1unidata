<?php
require_once dirname(__FILE__).'/DataStudent.php';
class DataStaff extends DataStudent {
	function __construct() {
		parent::__construct();
	}
	function getProfile() {
		parent::getProfile();
		$this->_user['staf'] = true;
		return $this->_user;
	}
	function checkStaffs() {
		parent::checkUsers();
	}
	function findStaff($unid) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF) {
			$this->throw_debug('findStaff not-admin/mod error!');
		}
		return parent::findUser($unid);
	}
	function listStaffs() {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF) {
			$this->throw_debug('listStaffs not-admin/mod error!');
		}
		$prep = "SELECT id,unid,bkid,name,nick,type,flag FROM ".$this->_usrtab.
			" WHERE type=".USER_STAFF." OR type=".USER_ADMIN." ORDER BY unid";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute()) {
			$this->throw_debug('listStaffs execute error!');
		}
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']);
				$item['type'] = intval($item['type']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function createStaff($unid,$name,$nrid,$nick,$alvl=USER_STAFF) {
		if ($this->_dotype!==USER_ADMIN) {
			$this->throw_debug('createStaff not-admin error!');
		}
		$name = strtoupper($name);
		$alvl = intval($alvl);
		parent::createUser($unid,$nrid,$name,$nick,$alvl);
	}
	// course-staff assignment (current)
	function checkCourseStaff() {
		$table = "course_staff";
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"coid","type"=>"INTEGER"),
				array("name"=>"stid","type"=>"INTEGER"),
				array("name"=>"alvl","type"=>"INTEGER")
			);
			$tmore = array();
			array_push($tmore,"UNIQUE (coid,stid)",
				"FOREIGN KEY(coid) REFERENCES courses(id)",
				"FOREIGN KEY(stid) REFERENCES ".$this->_usrtab."(id)");
			$this->table_create($table,$tdata,$tmore);
		}
		$this->checkCourses();
		$this->checkStaffs();
		$view = "course_staff_view";
		if (!$this->view_exists($view)) {
			$data = array();
			array_push($data,
				array("which"=>"T2.code","alias"=>"course"),
				array("which"=>"T2.name","alias"=>"coursename"),
				array("which"=>"T3.unid","alias"=>"staff"),
				array("which"=>"T3.nick","alias"=>"staffname"),
				array("which"=>"T1.coid","alias"=>"coid"),
				array("which"=>"T1.stid","alias"=>"stid"),
				array("which"=>"T1.alvl","alias"=>"role")
			);
			$more = array();
			array_push($more," FROM course_staff T1, courses T2, ",
				$this->_usrtab." T3 WHERE T1.coid=T2.id AND T1.stid=T3.id");
			$this->view_create($view,$data,$more);
		}
	}
	function listCourseStaff($code=null,$user=null,$type=null) {
		if ($code!=null) $code = strtoupper($code);
		if ($user!=null) $user = strtoupper($user);
		if ($type!=null) {
			switch (intval($type)) {
				case COURSE_ADMIN: $type = COURSE_ADMIN; break;
				case COURSE_STAFF: $type = COURSE_STAFF; break;
				default:
					$this->throw_debug('Invalid role!');
			}
		}
		$prep = "SELECT * FROM course_staff_view";
		$temp = " role!=0";
		if ($code!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." course=:code";
		}
		if ($user!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." staff=:user";
		}
		if ($type!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." role=:type";
		}
		if ($temp!=="") $prep = $prep." WHERE".$temp;
		$prep = $prep." ORDER BY course, role, staff";
		$stmt = $this->prepare($prep);
		if ($code!=null) {
			if (!$stmt->bindValue(':code',$code,PDO::PARAM_STR)) {
				$this->throw_debug('listCourseStaff bind error!');
			}
		}
		if ($user!=null) {
			if (!$stmt->bindValue(':user',$user,PDO::PARAM_STR)) {
				$this->throw_debug('listCourseStaff bind error!');
			}
		}
		if ($type!=null) {
			if (!$stmt->bindValue(':type',$type,PDO::PARAM_INT)) {
				$this->throw_debug('listCourseStaff bind error!');
			}
		}
		if (!$stmt->execute()) {
			$this->throw_debug('listCourseStaff execute error!');
		}
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach($list as &$item) {
				$item['coid'] = intval($item['coid']);
				$item['stid'] = intval($item['stid']);
				$item['role'] = intval($item['role']);
			}
			$pack['list'] = $list;
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function createCourseStaff($coid,$stid,$alvl=COURSE_STAFF) {
		$coid = intval($coid);
		$stid = intval($stid);
		$alvl = intval($alvl);
		$prep = "SELECT id,alvl FROM course_staff WHERE ".
			"coid=:coid AND stid=:stid";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':coid',$coid,PDO::PARAM_INT);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		$temp = $stmt->execute();
		if ($temp===false) {
			$this->throw_debug('createCourseStaff execute error');
		}
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item===false) {
			// insert new
			$prep = "INSERT INTO course_staff (coid,stid,alvl) ".
				"VALUES (:coid,:stid,:alvl)";
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':coid',$coid,PDO::PARAM_INT);
			$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
			$stmt->bindValue(':alvl',$alvl,PDO::PARAM_INT);
			$temp = $stmt->execute();
			$stmt->closeCursor();
			if ($temp==false) {
				$this->throw_debug('createCourseStaff insert Failed');
			}
		} else {
			// modify existing
			$prep = "UPDATE course_staff SET alvl=:alvl WHERE id=:id";
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':alvl',$alvl,PDO::PARAM_INT);
			$stmt->bindValue(':id',$item['id'],PDO::PARAM_INT);
			$temp = $stmt->execute();
			$stmt->closeCursor();
			$item = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if ($temp==false) {
				$this->throw_debug('createCourseStaff update Failed');
			}
		}
	}
	// course-staff assignment in specific semester
	function checkCoursesStaffs() {
		$table = "courses_staffs";
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"ssem","type"=>"INTEGER"),
				array("name"=>"coid","type"=>"INTEGER"),
				array("name"=>"stid","type"=>"INTEGER"),
				array("name"=>"flag","type"=>"INTEGER")
			);
			$tmore = array();
			array_push($tmore,"UNIQUE (ssem,coid,stid)",
				"FOREIGN KEY(coid) REFERENCES courses (id)",
				"FOREIGN KEY(stid) REFERENCES users (id)");
			$this->table_create($table,$tdata,$tmore);
		}
		$this->checkStaffs();
		$this->checkCourses();
		$view = "courses_staffs_view";
		if (!$this->view_exists($view)) {
			$data = array();
			array_push($data,
				array("which"=>"T1.ssem","alias"=>"ssem"),
				array("which"=>"T2.code","alias"=>"course"),
				array("which"=>"T2.name","alias"=>"coursename"),
				array("which"=>"T3.unid","alias"=>"staff"),
				array("which"=>"T3.name","alias"=>"staffname"),
				array("which"=>"T3.nick","alias"=>"staffnick"),
				array("which"=>"T1.coid","alias"=>"coid"),
				array("which"=>"T1.stid","alias"=>"stid"),
				array("which"=>"T1.flag","alias"=>"role")
			);
			$more = array();
			array_push($more," FROM courses_staffs T1, courses T2, ",
				$this->_usrtab." T3 WHERE T1.coid=T2.id AND T1.stid=T3.id");
			$this->view_create($view,$data,$more);
		}
	}
	function createCoursesStaffs($coid,$stid,$ssem,$alvl=COURSE_CHECK) {
		$coid = intval($coid);
		$stid = intval($stid);
		$ssem = intval($ssem);
		$prep = "SELECT id,flag FROM courses_staffs WHERE ".
			"coid=:coid AND stid=:stid AND ssem=:ssem";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':coid',$coid,PDO::PARAM_INT);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		$stmt->bindValue(':ssem',$ssem,PDO::PARAM_INT);
		$temp = $stmt->execute();
		if ($temp===false) {
			$this->throw_debug('createCoursesStaffs execute error');
		}
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($item===false) {
			// get level from course_staff
			$prep = "SELECT id,alvl FROM course_staff WHERE ".
				"coid=:coid AND stid=:stid";
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':coid',$coid,PDO::PARAM_INT);
			$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
			$temp = $stmt->execute();
			if ($temp===false) {
				$this->throw_debug('createCoursesStaffs execute2 error');
			}
			$item = $stmt->fetch(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			if ($item===false) {
				$alvl = COURSE_STAFF;
			} else {
				$alvl = intval($item['alvl']);
			}
			// create new
			$prep = "INSERT INTO courses_staffs (ssem,coid,stid,flag) ".
				"VALUES (:ssem,:coid,:stid,:alvl)";
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':ssem',$ssem,PDO::PARAM_INT);
			$stmt->bindValue(':coid',$coid,PDO::PARAM_INT);
			$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
			$stmt->bindValue(':alvl',$alvl,PDO::PARAM_INT);
			$temp = $stmt->execute();
			$stmt->closeCursor();
			if ($temp==false) {
				$this->throw_debug('createCoursesStaffs insert Failed');
			}
		} else {
			$alvl = intval($alvl);
			switch ($alvl) {
				case COURSE_ADMIN:
				case COURSE_STAFF:
				case COURSE_CHECK:
					break;
				default:
					$alvl = COURSE_CHECK;
			}
			// modify existing role
			$prep = "UPDATE courses_staffs SET flag=:alvl WHERE id=:id";
			$stmt = $this->prepare($prep);
			$stmt->bindValue(':alvl',$alvl,PDO::PARAM_INT);
			$stmt->bindValue(':id',$item['id'],PDO::PARAM_INT);
			$temp = $stmt->execute();
			$stmt->closeCursor();
			$item = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if ($temp==false) {
				$this->throw_debug('createCoursesStaffs update Failed');
			}
		}
	}
	// course implementations
	function listCourseImps() {
		$pack = [];
		$prep = "select name,substr(tbl_name,-9) as ssem from sqlite_master ".
			"where type='table' and name glob '*_[0-9]????????' order by ".
			"ssem desc, name";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute()) {
			$this->throw_debug('listCourseImps execute error!');
		}
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['list'] = $list;
			$pack['stat'] = true;
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function removeCourseImps($code,$ssem) {
		if ($this->_dotype!==USER_ADM) {
			$this->throw_debug('modifyCoursesComponents not-admin/mod error!');
		}
		// remove table (course_student info here!)
		$name = $code."_".$ssem;
		$prep = "drop table $name";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute()) {
			$this->throw_debug('removeCourseImps remove table error!');
		}
		$stmt->closeCursor();
		// check course and get id
		$test = $this->findCourse($code);
		if ($test['stat']!==true) {
			$this->throw_debug('Invalid course?!');
		}
		$coid = intval($test['id']);
		$ssem = intval($ssem);
		// remove related courses_components entries
		$prep = "DELETE FROM courses_components ".
			"WHERE ssem=:ssem AND coid=:coid";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(':ssem',$ssem,PDO::PARAM_INT)) {
			$this->throw_debug('removeCourseImps bind error!');
		}
		if (!$stmt->bindValue(':coid',$coid,PDO::PARAM_INT)) {
			$this->throw_debug('removeCourseImps bind error!');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('removeCourseImps exec error');
		}
		$stmt->closeCursor();
		// update courses_staffs (COURSE_CHECK)
		$list = $this->listCoursesStaffs(null,$code,$ssem);
		if ($list['stat']!==true) {
			$this->throw_debug('removeCourseImps list error');
		}
		foreach ($list['list'] as $item) {
			$this->createCoursesStaffs($item['coid'],$item['stid'],
				$item['ssem'],COURSE_CHECK);
		}
	}
}
?>
