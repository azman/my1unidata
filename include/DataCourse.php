<?php
require_once dirname(__FILE__).'/Data.php';
define('USER_ADMIN',USER_ADM);
define('USER_STAFF',USER_MOD);
define('USER_STUDENT',USER_USR);
define('FLAG_INACTIVE',0x8000);
define('FLAG_GRADUATED',0x0001|FLAG_INACTIVE);
define('FLAG_SUSPENDED',0x0002|FLAG_INACTIVE);
define('FLAG_TERMINATED',0x0004|FLAG_INACTIVE);
define('EVAL_NONE',0);
define('EVAL_INIT',1);
define('EVAL_DONE',2);
class DataCourse extends Data {
	protected $_dofull; // validate results - temp?
	protected $_sessem; // session semester YYYYNNNNS format
	protected $_user;
	function __construct() {
		parent::__construct();
		$this->_dofull = null;
		$this->_sessem = null;
		$this->_user = null;
	}
	static function get_grade($mark) {
		$mark = floatval($mark);
		if ($mark>100.0||$mark<0.0) return 'X';
		else if ($mark<30.0) return 'F';
		else if ($mark<35.0) return 'D-';
		else if ($mark<40.0) return 'D';
		else if ($mark<45.0) return 'D+';
		else if ($mark<50.0) return 'C-';
		else if ($mark<55.0) return 'C';
		else if ($mark<60.0) return 'C+';
		else if ($mark<65.0) return 'B-';
		else if ($mark<70.0) return 'B';
		else if ($mark<75.0) return 'B+';
		else if ($mark<80.0) return 'A-';
		else return 'A';
	}
	function getProfile() {
		$this->_user = parent::getProfile();
		return $this->_user;
	}
	function is_valid_user() {
		if ($this->_user===null||!isset($this->_user['staf']))
			return false;
		return true;
	}
	function is_staff() {
		if ($this->_user===null||$this->_user['type']>USER_STAFF)
			return false;
		return true;
	}
	function getSessionLabel($sessem) {
		$year1 = intval($sessem/100000);
		$year2 = intval(($sessem%100000)/10);
		$dosem = intval($sessem%10);
		return "Academic Session ".$year1."/".$year2." Semester ".$dosem;
	}
	function selectSession($sessem) {
		$year1 = intval($sessem/100000);
		$year2 = intval(($sessem%100000)/10);
		$dosem = intval($sessem%10);
		if ($year2!=($year1+1)||$dosem>3||$dosem<1)
			$this->throw_debug('Invalid Session/Semester! ('.$sessem.')');
		$this->_sessem = intval($sessem);
		$check = "Academic Session ".$year1."/".$year2;
		$check = $check." Semester ".$dosem;
		return $check;
	}
	function checkCourses() {
		$table = "courses";
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"code","type"=>"TEXT UNIQUE NOT NULL"),
				array("name"=>"name","type"=>"TEXT NOT NULL"),
				array("name"=>"unit","type"=>"INTEGER"),
				array("name"=>"asem","type"=>"INTEGER"),
				array("name"=>"flag","type"=>"INTEGER")
			);
			$this->table_create($table,$tdata);
		}
	}
	function findCourse($code) {
		$prep = "SELECT id, name, unit, asem, flag FROM courses WHERE code=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$code,PDO::PARAM_STR))
			$this->throw_debug('findCourse bind error!');
		if (!$stmt->execute())
			$this->throw_debug('findCourse execute error!');
		$pack = [];
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item!=false) {
			$pack = $item;
			$pack['stat'] = true;
			$pack['id'] = intval($item['id']); // make sure an integer?
			$pack['unit'] = intval($item['unit']);
			$pack['asem'] = intval($item['asem']);
			$pack['flag'] = intval($item['flag']);
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function listCourses() {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF) {
			$this->throw_debug('listCourses not-admin/mod error!');
		}
		$prep = "SELECT code,name,unit,asem,flag FROM courses ORDER BY code";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute()) {
			$this->throw_debug('listCourses execute error!');
		}
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['unit'] = intval($item['unit']);
				$item['asem'] = intval($item['asem']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function createCourse($code,$name,$unit,$asem,$flag=1) {
		$code = strtoupper($code);
		$name = strtoupper($name);
		$unit = intval($unit);
		$asem = intval($asem);
		$flag = intval($flag);
		$prep = "INSERT INTO courses (code,name,unit,asem,flag) ".
			"VALUES (:code,:name,:unit,:asem,:flag)";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(':code',$code,PDO::PARAM_STR)||
				!$stmt->bindValue(':name',$name,PDO::PARAM_STR)||
				!$stmt->bindValue(':unit',$unit,PDO::PARAM_INT)||
				!$stmt->bindValue(':asem',$asem,PDO::PARAM_INT)||
				!$stmt->bindValue(':flag',$flag,PDO::PARAM_INT)) {
			$this->throw_debug('createCourse bind error');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('createCourse execute error');
		}
		$stmt->closeCursor();
	}
	function modifyCourse($id,$code,$name,$unit,$asem,$flag=1) {
		$code = strtoupper($code);
		$name = strtoupper($name);
		$unit = intval($unit);
		$asem = intval($asem);
		$id = intval($id);
		$prep = "UPDATE courses SET code=?,name=?,unit=?,".
			"asem=?,flag=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$code,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$name,PDO::PARAM_STR)||
				!$stmt->bindValue(3,$unit,PDO::PARAM_INT)||
				!$stmt->bindValue(4,$asem,PDO::PARAM_INT)||
				!$stmt->bindValue(5,$flag,PDO::PARAM_INT)||
				!$stmt->bindValue(6,$id,PDO::PARAM_INT)) {
			$this->throw_debug('modifyCourse bind error!');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('modifyCourse execute error!');
		}
	}
	function checkCoursesComponents() {
		$table = "courses_components";
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"ssem","type"=>"INTEGER"),
				array("name"=>"coid","type"=>"INTEGER"),
				array("name"=>"name","type"=>"TEXT"),
				array("name"=>"lbl","type"=>"TEXT"),
				array("name"=>"raw","type"=>"REAL"),
				array("name"=>"pct","type"=>"REAL"),
				array("name"=>"grp","type"=>"INTEGER"),
				array("name"=>"sub","type"=>"INTEGER"),
				array("name"=>"idx","type"=>"INTEGER"),
				array("name"=>"flag","type"=>"INTEGER")
			);
			$tmore = array();
			array_push($tmore,"UNIQUE (ssem,coid,name)",
				"FOREIGN KEY(coid) REFERENCES courses(id)");
			$this->table_create($table,$tdata,$tmore);
		}
	}
	function listCoursesComponents($coid,$name=null) {
		if ($this->_sessem==null) {
			$this->throw_debug('Session/Semester NOT selected!');
		}
		$prep = "SELECT id, name, lbl, raw, pct, grp, sub, idx, flag";
		$prep = $prep." FROM courses_components";
		$prep = $prep." WHERE coid=? AND ssem=?";
		if ($name!=null) {
			$prep = $prep." AND name=?";
			$name = strtoupper($name);
		}
		$prep = $prep." ORDER BY grp, sub, idx";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$coid,PDO::PARAM_INT)||
				!$stmt->bindValue(2,$this->_sessem,PDO::PARAM_INT)) {
			$this->throw_debug('listCoursesComponents bind error!');
		}
		if ($name!=null) {
			if (!$stmt->bindValue(3,$name,PDO::PARAM_STR)) {
				$this->throw_debug('listCoursesComponents bind error!');
			}
		}
		if (!$stmt->execute()) {
			$this->throw_debug('listCoursesComponents execute error!');
		}
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']); // make sure an integer?
				$item['raw'] = floatval($item['raw']);
				$item['pct'] = floatval($item['pct']);
				$item['grp'] = intval($item['grp']);
				$item['sub'] = intval($item['sub']);
				$item['idx'] = intval($item['idx']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		} else {
			$pack['stat'] = false;
		}
		return $pack;
	}
	function createCoursesComponents($coid,$name,$raw,$pct,
			$lab,$grp,$sub,$idx,$flag=1) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF) {
			$this->throw_debug('createCoursesComponents not-admin/mod error!');
		}
		if ($this->_sessem==null) {
			$this->throw_debug('Session/Semester NOT selected!');
		}
		$coid = intval($coid);
		$name = strtoupper($name);
		$lab = strtoupper($lab);
		$raw = floatval($raw);
		$pct = floatval($pct);
		$grp = intval($grp);
		$sub = intval($sub);
		$idx = intval($idx);
		$flag = intval($flag);
		$prep = "INSERT INTO courses_components ";
		$prep = $prep."(ssem,coid,name,lbl,raw,pct,grp,sub,idx,flag) VALUES ";
		$prep = $prep."(:ssem,:coid,:name,:lab,:raw,:pct,:grp,:sub,:idx,:flag)";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(':ssem',$this->_sessem,PDO::PARAM_INT)||
				!$stmt->bindValue(':coid',$coid,PDO::PARAM_INT)||
				!$stmt->bindValue(':name',$name,PDO::PARAM_STR)||
				!$stmt->bindValue(':lab',$lab,PDO::PARAM_STR)||
				!$stmt->bindValue(':raw',$raw,PDO::PARAM_STR)||
				!$stmt->bindValue(':pct',$pct,PDO::PARAM_STR)||
				!$stmt->bindValue(':grp',$grp,PDO::PARAM_INT)||
				!$stmt->bindValue(':sub',$sub,PDO::PARAM_INT)||
				!$stmt->bindValue(':idx',$idx,PDO::PARAM_INT)||
				!$stmt->bindValue(':flag',$flag,PDO::PARAM_INT)) {
			$this->throw_debug('createCoursesComponents bind error!');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('createCoursesComponents execute error!');
		}
		$stmt->closeCursor();
	}
	function modifyCoursesComponents($ccid,$name,$text,$rawm,$pctm,
			$grpm,$subm,$idxm) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF) {
			$this->throw_debug('modifyCoursesComponents not-admin/mod error!');
		}
		$ccid = intval($ccid);
		$name = strtoupper($name);
		$text = strtoupper($text);
		$rawm = floatval($rawm);
		$pctm = floatval($pctm);
		$grpm = intval($grpm);
		$subm = intval($subm);
		$idxm = intval($idxm);
		$prep = "UPDATE courses_components SET name=?,".
			"lbl=?,raw=?,pct=?,grp=?,sub=?,idx=? WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$name,PDO::PARAM_STR)||
				!$stmt->bindValue(2,$text,PDO::PARAM_STR)||
				!$stmt->bindValue(3,$rawm,PDO::PARAM_STR)||
				!$stmt->bindValue(4,$pctm,PDO::PARAM_STR)||
				!$stmt->bindValue(5,$grpm,PDO::PARAM_INT)||
				!$stmt->bindValue(6,$subm,PDO::PARAM_INT)||
				!$stmt->bindValue(7,$idxm,PDO::PARAM_INT)||
				!$stmt->bindValue(8,$ccid,PDO::PARAM_INT)) {
			$this->throw_debug('modifyCoursesComponents bind error!');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('modifyCoursesComponents execute error!');
		}
		$stmt->closeCursor();
	}
}
?>
