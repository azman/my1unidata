<?php
// all ratings
$opts_rateX = [["1","Strongly Disagree"],["2","Disagree"],
	["3","Neither agree or disagree"],["4","Agree"],
	["5","Strongly Agree"]];
$text_partA = "<b>PART A: COURSE EVALUATION</b>";
$opts_partA = [
	"A1" =>
	"The course has developed my interest in the subject.",
	"A2" =>
	"The course has given me a good understanding of the subject.",
	"A3" =>
	"The course was well organized.",
	"A4" =>
	"The handouts/exercises/assessments given were adequate.",
	"A5" =>
	"The workshops/seminars/tutorials/practices/etc. were useful.",
	"A6" =>
	"I have achieved Course Outcome No. 1 (CO1)",
	"A7" =>
	"I have achieved Course Outcome No. 2 (CO2)",
	"A8" =>
	"I have achieved Course Outcome No. 3 (CO3)",
	"A9" =>
	"I have achieved Course Outcome No. 4 (CO4)"
];
$text_partB = "<b>PART B: TEACHING EVALUATION</b>";
$opts_partB = [
	"B1" =>
	"The course syllabus was presented clearly to the class.",
	"B2" =>
	"The lecturer demonstrated knowledge and competence in ".
		"the subject matter.",
	"B3" =>
	"The lecturer gave sufficient practices to test my ".
		"understanding of concepts and principles.",
	"B4" =>
	"Corrected assignments and tests were made available ".
		"to the students.",
	"B5" =>
	"The lecturer was punctual for classes.",
	"B6" =>
	"The lecturer is easily available for consultation.",
	"B7" =>
	"Overall, I would rate the lecturer's performance as very good."
];
$text_partC = "<b>PART C: LAB EVALUATION</b>";
$opts_partC = [
	"C1" =>
	"Labs help me in understanding course concepts and ".
		"principles (theory).",
	"C2" =>
	"Labs help me in understanding application of theory.",
	"C3" =>
	"Lab tests and presentation help me in understanding course ".
		"theory.",
	"C4" =>
	"The Lecturer/PLV is very helpful.",
	"C5" =>
	"The Lecturer/PLV is punctual for labs.",
	"C6" =>
	"The Lecturer/PLV demonstrated knowledge & competence in the ".
		"subject matter.",
	"C7" =>
	"Corrected lab reports and test were made available to the ".
		"students.",
	"C8" =>
	"Overall, I would rate the Lecturer/PLV's performance as very good."
];
?>
