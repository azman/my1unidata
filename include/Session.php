<?php
require_once dirname(__FILE__).'/config.php';
class Session {
	function __construct() {
		$this->Start();
	}
	function Start() {
		if (session_status()===PHP_SESSION_NONE)
			session_start();
	}
	function Validate() {
		if (!isset($_SESSION[SESSION_USER])||!isset($_SESSION[SESSION_PASS]))
			return false;
		return true;
	}
	function Clean() {
		//$this->Start(); // always true for an instance!
		session_unset();
		session_destroy();
	}
	function TimeCheck() {
		//$this->Start(); // always true for an instance!
		if (isset($_SESSION[SESSION_TIME])&&
				(time()-$_SESSION[SESSION_TIME]>_SESSION[SESSION_TIMEOUT])) {
			session_unset();
			session_destroy();
		}
		$_SESSION[SESSION_TIME] = time();
	}
	function StoreLogin($user,$pass) {
		$_SESSION[SESSION_USER] = $user;
		$_SESSION[SESSION_PASS] = $pass;
	}
	function CheckLogin($user,$pass) {
		if (($user!==$_SESSION[SESSION_USER])||
				($pass!==$_SESSION[SESSION_PASS]))
			return false;
		return true;
	}
}
?>
