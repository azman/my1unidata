<?php
function define_if_not_exists($stuff,$value) {
	defined($stuff) or define($stuff,$value);
}
// get top path
define('MY1APP_PATH',realpath(dirname(__FILE__).'/../'));
// allow local config file to override
define('MY1CFGFILE',(__FILE__).'.local');
if (file_exists(MY1CFGFILE)) require_once MY1CFGFILE;
//  for install.php to create a local version
define_if_not_exists('MY1CFGINIT',MY1CFGFILE);
// application general contants
define_if_not_exists('DEBUG_MODE',false);
define_if_not_exists('EMESG_MODE',false);
define_if_not_exists('EMESG_MASK','General Error!');
define_if_not_exists('LOGIN_MODE',true);
define_if_not_exists('MY1APP_TITLE','MY1SrvApp');
define_if_not_exists('MY1APP_USERS_TABLE','users');
define_if_not_exists('RESOURCE_PREFIX','resources/');
define_if_not_exists('SESSION_PREFIX','my1srvapp');
define_if_not_exists('SESSION_USER',SESSION_PREFIX.'_username');
define_if_not_exists('SESSION_PASS',SESSION_PREFIX.'_password');
define_if_not_exists('SESSION_TIME','LASTACTIVE');
define_if_not_exists('SESSION_TIMEOUT',1800); // in seconds (30min)
// user levels
define_if_not_exists('USER_NOT',0x00);
define_if_not_exists('USER_ADM',0x01);
define_if_not_exists('USER_MOD',0x02);
define_if_not_exists('USER_USR',0x03);
// installation stuff
define_if_not_exists('DEFAULT_ROOT_UNID','root');
define_if_not_exists('DEFAULT_ROOT_BKID','toor');
define_if_not_exists('DEFAULT_ROOT_NAME','ROOT');
// Base constants
define_if_not_exists('DEFAULT_DBPATH',MY1APP_PATH.'/data');
// Data constants
define_if_not_exists('DEFAULT_DATA_CLASS','Data');
define_if_not_exists('DATA_FILE','data.sqlite');
// preparing for mariadb
define_if_not_exists('MARIADB_NAME','kingdb');
define_if_not_exists('MARIADB_USER','king');
define_if_not_exists('MARIADB_PASS','king');
// FileText constants
define_if_not_exists('DEFAULT_TFPATH',DEFAULT_DBPATH);
define_if_not_exists('DEFAULT_TFNAME','data.txt');
define_if_not_exists('MAX_LINESIZE',1000);
define_if_not_exists('CSV_WITH_HEADER',1);
define_if_not_exists('CSV_WITHOUT_HEADER',0);
// default pages
define_if_not_exists('DEFAULT_THEME','ThemeNone');
define_if_not_exists('SHOW_INIT',true);
define_if_not_exists('PAGE_INIT','PageInit');
define_if_not_exists('PAGE_MAIN','PageMain');
define_if_not_exists('PAGE_LOGIN','PageLogin');
// task handlers
define_if_not_exists('TASK_GET','TaskGET');
define_if_not_exists('TASK_POST','TaskPOST');
// constants
define_if_not_exists('SINGLE_BACK',1);
define_if_not_exists('DOUBLE_BACK',2);
define_if_not_exists('DEFAULT_BACK_COUNT',SINGLE_BACK);
define_if_not_exists('MENUITEM_CMD_',0);
define_if_not_exists('MENUITEM_LINK',1);
?>
