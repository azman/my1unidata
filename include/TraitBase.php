<?php
trait TraitBase {
	protected $_validate;
	protected $_dbdata;
	protected $_dodata;
	function initialize_base($mode,$data) {
		if (!isset($this->_validate))
			$this->_validate = $mode; //LOGIN_MODE;
		if (!isset($this->_dbdata))
			$this->_dbdata = $data; //DEFAULT_DATA_CLASS;
		$this->initialize_data();
		if (method_exists($this,'data_upgrade'))
			$this->data_upgrade();
	}
	function initialize_data() {
		$cdata = $this->_dbdata;
		require_once dirname(__FILE__).'/../include/'.$cdata.'.php';
		$this->_dodata = new $cdata();
		if ($this->_validate === true) {
			require_once dirname(__FILE__).'/../include/Session.php';
			$session = new Session();
			if ($session->Validate()!==true)
				$this->throw_debug('Invalid Session!');
			$pass = $this->_dodata->validate($_SESSION[SESSION_USER],
				$_SESSION[SESSION_PASS]);
			if ($pass!==true) {
				session_destroy();
				$this->throw_debug('Invalid login!');
			}
		}
	}
}
?>
