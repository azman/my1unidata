<?php
require_once dirname(__FILE__).'/DataCourse.php';
class DataStudent extends DataCourse {
	function __construct() {
		parent::__construct();
	}
	static function is_active($item) {
		$akey = 'flag';
		if (!is_array($item)||!array_key_exists($akey,$item))
			return null;
		return (($item[$akey]&STUDENT_FLAG_MASK)===STUDENT_FLAG_ACTIVE);
	}
	static function was_active($item) {
		$akey = 'flag';
		if (!is_array($item)||!array_key_exists($akey,$item))
			return null;
		return (($item[$akey]&STUDENT_FLAG_DROP)===STUDENT_FLAG_DROP);
	}
	function getProfile() {
		parent::getProfile();
		$this->_user['staf'] = false;
		return $this->_user;
	}
	function getStaffName($id) {
		$prep = "SELECT unid,name FROM ".$this->_usrtab." WHERE id=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$id,PDO::PARAM_INT))
			$this->throw_debug('getStaffName bind error!');
		if (!$stmt->execute())
			$this->throw_debug('getStaffName exec error!');
		$pack = [];
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item!=false) {
			$pack['stat'] = true;
			$pack['unid'] = $item['unid'];
			$pack['name'] = $item['name'];
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function checkProgs() {
		$table = "progs";
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"prog","type"=>"TEXT UNIQUE NOT NULL"),
				array("name"=>"name","type"=>"TEXT NOT NULL"));
			$this->table_create($table,$tdata);
		}
	}
	function findProg($prog,$id=null) {
		$prep = "SELECT id,prog,name FROM progs ";
		if ($id!==null) {
			$prep = $prep."WHERE id=?";
			$stmt = $this->prepare($prep);
			if (!$stmt->bindValue(1,$id,PDO::PARAM_INT))
				$this->throw_debug('findProg bind1 error!');
		}
		else {
			$prep = $prep." WHERE prog=?";
			$stmt = $this->prepare($prep);
			if (!$stmt->bindValue(1,$prog,PDO::PARAM_STR))
				$this->throw_debug('findProg bind2 error!');
		}
		if (!$stmt->execute())
			$this->throw_debug('findProg execute error!');
		$pack = [];
		$item = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($item!=false) {
			$pack['id'] = intval($item['id']); // make sure an integer?
			$pack['prog'] = $item['prog'];
			$pack['name'] = $item['name'];
		}
		else {
			$pack['id'] = 0;
			$pack['prog'] = "Unknown";
			$pack['name'] = "Unknown";
		}
		return $pack;
	}
	function createProg($prog,$name) {
		if ($this->_dotype!==USER_ADMIN)
			$this->throw_debug('createProg not-admin/mod error!');
		$prog = strtoupper(trim($prog));
		$name = strtoupper(trim($name));
		// find existing
		$item = $this->findProg($prog);
		if ($item['stat']===true)
			$this->throw_debug('createProg exists error!');
		$prep = "INSERT INTO progs (prog,name) VALUES (:prog,:name)";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':prog',$prog,PDO::PARAM_STR);
		$stmt->bindValue(':name',$name,PDO::PARAM_STR);
		if (!$stmt->execute())
			$this->throw_debug('createProg execute error!');
		$stmt->closeCursor();
	}
	function checkStudents() {
		parent::checkUsers();
	}
	function findStudent($unid) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('findStudent not-admin/mod error!');
		return parent::findUser($unid);
	}
	function listStudents() {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('listStudents not-admin/mod error!');
		$prep = "SELECT id,unid,bkid,name,nick,type,flag FROM ".$this->_usrtab.
			" WHERE type=".USER_STUDENT." ORDER BY unid ASC";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_debug('listStudents execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']);
				$item['type'] = intval($item['type']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function createStudent($unid,$name,$nrid,$prog) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('createStudent not-admin/mod error!');
		$unid = strtoupper(trim($unid));
		$name = strtoupper(trim($name));
		$nrid = strtoupper(trim($nrid));
		$prog = intval($prog);
		parent::createUser($unid,$nrid,$name,$name,USER_STUDENT,$prog);
	}
	function modifyStudent($unid,$prog) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('modifyStudent not-admin/mod error!');
		$unid = strtoupper(trim($unid));
		$prog = intval($prog);
		$prep = "UPDATE ".$this->_usrtab." SET flag=? WHERE unid=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$prog,PDO::PARAM_INT)||
				!$stmt->bindValue(2,$unid,PDO::PARAM_STR))
			$this->throw_debug('modifyStudent bind error!');
		if (!$stmt->execute())
			$this->throw_debug('modifyStudent execute error!');
		$stmt->closeCursor();
	}
	function findStudentCourses($stid,$ssem) {
		// how to get all courses taken?
		$prep = "SELECT name FROM sqlite_master WHERE type='table' and";
		$prep = $prep." name glob '*_".$ssem."'";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_debug('findStudentCourses execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if ($list!==false) {
			$pack['stat'] = true;
			$list_that = [];
			foreach ($list as $item) {
				$part = explode('_',$item['name']);
				if ($part[1]!==$ssem) continue;
				$prep = "SELECT * FROM ".$item['name']." WHERE stid=?";
				$stmt = $this->prepare($prep);
				if (!$stmt->bindValue(1,$stid,PDO::PARAM_INT))
					$this->throw_debug('findStudentCoursesl bind error!');
				if (!$stmt->execute())
					$this->throw_debug('findStudentCoursesl execute error!');
				$temp = $stmt->fetch(PDO::FETCH_ASSOC);
				$stmt->closeCursor();
				if ($temp!==false) {
					$that = $this->findCourse($part[0]);
					if ($that['stat']===false)
						$that['name'] = "Unknown";
					$temp['coid'] = $that['id'];
					$temp['code'] = $part[0];
					$temp['name'] = $that['name'];
					array_push($list_that,$temp);
				}
			}
			$pack['list'] = $list_that;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function checkCourseStudent($table,$coid) {
		$temp = $this->listCoursesComponents($coid);
		if ($temp['stat']==false)
			$this->throw_debug('checkCourseStudent find error!');
		if (!$this->table_exists($table)) {
			// create new table
			$tdata = array();
			// common columns
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"stid","type"=>"INTEGER UNIQUE"),
				array("name"=>"lgrp","type"=>"TEXT"),
				array("name"=>"mgrp","type"=>"TEXT"),
				array("name"=>"flag","type"=>"INTEGER"));
			// course component columns
			foreach ($temp['list'] as $item) {
				$name = preg_replace('/\s+/','_',$item['name']);
				$name = strtolower(preg_replace('/-/','_',$name));
				array_push($tdata,array("name"=>$name,"type"=>"REAL"));
			}
			$tmore = array();
			array_push($tmore,
				"FOREIGN KEY(stid) REFERENCES ".$this->_usrtab."(id)");
			$this->table_create($table,$tdata,$tmore);
		}
		else {
			// update table columns
			$find = array();
			$prep = "pragma table_info(".$table.")";
			$stmt = $this->query($prep);
			$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if ($list===false)
				$this->throw_debug('checkCourseStudent getinfo error');
			$stmt->closeCursor();
			foreach ($list as $item)
				array_push($find,$item['name']);
			// check columns
			$tdata = array();
			foreach ($temp['list'] as $item) {
				$name = preg_replace('/\s+/','_',$item['name']);
				$name = strtolower(preg_replace('/-/','_',$name));
				if (array_search($name,$find)===false)
					array_push($tdata,array("name"=>$name,"type"=>"REAL"));
			}
			if (!empty($tdata))
				$this->table_addcol($table,$tdata);
		}
	}
	function findCourseStudent($table,$stid=null,$flag=null) {
		$prep = "SELECT id,stid,lgrp,mgrp,flag FROM ".$table;
		$next = "";
		if ($stid!=null) {
			$next = " stid=:stid";
			$stid = intval($stid);
		}
		if ($flag!=null) {
			if ($next!="") $next = $next." AND";
			$next = $next." flag=:flag";
			$flag = intval($flag);
		}
		if ($next!="") $prep = $prep." WHERE".$next;
		$stmt = $this->prepare($prep);
		if ($stid!=null) {
			if (!$stmt->bindValue(':stid',$stid,PDO::PARAM_INT))
				$this->throw_debug('findCourseStudent bind error!');
		}
		if ($flag!=null) {
			if (!$stmt->bindValue(':flag',$flag,PDO::PARAM_INT))
				$this->throw_debug('findCourseStudent bind error!');
		}
		if (!$stmt->execute())
			$this->throw_debug('findCourseStudent execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']);
				$item['stid'] = intval($item['stid']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function listCourseStudent($table) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('listCourseStudent not-admin/mod error!');
		$prep = "SELECT T2.name, T2.unid, T2.bkid, ".
			"T2.flag as prog, T1.* FROM ".$table." T1, ".
			$this->_usrtab." T2 WHERE T1.stid=T2.id ORDER BY T2.name";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_debug('listCourseStudent execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['stid'] = intval($item['stid']);
				$item['flag'] = intval($item['flag']);
			}
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function createCourseStudent($table,$stid,$lgrp,$mgrp,
			$flag=STUDENT_FLAG_ACTIVE) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('createCourseStudent not-admin/mod error!');
		$stid = intval($stid);
		$lgrp = strtoupper(trim($lgrp));
		$mgrp = strtoupper(trim($mgrp));
		$flag = intval($flag);
		$list = $this->findCourseStudent($table,$stid);
		if ($list['stat']===true&&count($list['list'])>0) {
			// modify!
			$prep = "UPDATE ".$table." SET lgrp=:lgrp,mgrp=:mgrp,flag=:flag";
			$prep = $prep." WHERE stid=:stid";
		}
		else {
			$prep = "INSERT INTO ".$table;
			$prep = $prep." (stid,lgrp,mgrp,flag)";
			$prep = $prep." VALUES (:stid,:lgrp,:mgrp,:flag)";
		}
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		$stmt->bindValue(':lgrp',$lgrp,PDO::PARAM_STR);
		$stmt->bindValue(':mgrp',$mgrp,PDO::PARAM_STR);
		$stmt->bindValue(':flag',$flag,PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_debug('createCourseStudent exec error');
		$stmt->closeCursor();
	}
	function modifyCourseStudent($table,$stid,$flag=STUDENT_FLAG_DROP) {
		// by default drop student from course
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('modifyCourseStudent not-admin/mod error!');
		$prep = "UPDATE ".$table." SET flag=".$flag." WHERE stid=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$stid,PDO::PARAM_INT))
			$this->throw_debug('modifyCourseStudent bind error!');
		if (!$stmt->execute())
			$this->throw_debug('modifyCourseStudent execute error!');
		$stmt->closeCursor();
	}
	function updateCourseStudentMark($table,$stid,$col,$val) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('updateCourseStudentMark not-admin/mod error!');
		$prep = "UPDATE ".$table." SET ".$col."=".$val." WHERE stid=?";
		$stmt = $this->prepare($prep);
		if (!$stmt->bindValue(1,$stid,PDO::PARAM_INT))
			$this->throw_debug('updateCourseStudentMark bind error!');
		if (!$stmt->execute())
			$this->throw_debug('updateCourseStudentMark execute error!');
		$stmt->closeCursor();
	}
	// needed here for pagestudenteval?
	function listCoursesStaffs($user=null,$code=null,$ssem=null) {
		if ($user!=null) $user = strtoupper($user);
		if ($code!=null) $code = strtoupper($code);
		if ($ssem!=null) $ssem = intval($ssem);
		$prep = "SELECT * FROM courses_staffs_view";
		$temp = " role!=0";
		if ($user!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." staff=:user";
		}
		if ($code!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." course=:code";
		}
		if ($ssem!=null) {
			if ($temp!=="") $temp = $temp." AND";
			$temp = $temp." ssem=:ssem";
		}
		if ($temp!="") $prep = $prep." WHERE".$temp;
		$prep = $prep." ORDER BY ssem DESC, course, role, staff";
		$stmt = $this->prepare($prep);
		if ($user!=null) {
			if (!$stmt->bindValue(':user',$user,PDO::PARAM_STR))
				$this->throw_debug('listCoursesStaffs bind error!');
		}
		if ($code!=null) {
			if (!$stmt->bindValue(':code',$code,PDO::PARAM_STR))
				$this->throw_debug('listCoursesStaffs bind error!');
		}
		if ($ssem!=null) {
			if (!$stmt->bindValue(':ssem',$ssem,PDO::PARAM_INT))
				$this->throw_debug('listCoursesStaffs bind error!');
		}
		if (!$stmt->execute()) {
			$this->throw_debug('listCoursesStaffs execute error!');
		}
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!=false) {
			$pack['stat'] = true;
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	// check if eval ready
	function testEvalReady($code,$ssem) {
		$table = $code.'_'.$ssem;
		$teval = $table.'_EVAL';
		$tstat = $table.'_EVALSTAT';
		if (!$this->table_exists($teval)||!$this->table_exists($tstat))
			return EVAL_NONE;
		if ($this->_dotype===USER_ADMIN||$this->_dotype===USER_STAFF)
			return EVAL_DONE;
		$find = $this->listCourseEvalStat($tstat,$this->_userid);
		if ($find['stat']===false||!count($find['list']))
			return EVAL_NONE;
		$find = $find['list'][0];
		if ($find['stat']!==EVAL_INIT)
			return EVAL_DONE;
		return EVAL_INIT;
	}
	// make eval ready
	function makeEvalReady($code,$ssem) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('makeEvalReady not-admin/mod error!');
		$table = $code.'_'.$ssem;
		$teval = $table.'_EVAL';
		$tstat = $table.'_EVALSTAT';
		$this->checkCourseEval($teval);
		$this->checkCourseEvalStat($tstat);
		// mark all student
		$list = $this->listCourseStudent($table);
		if ($list['stat']!==true)
			$this->throw_debug('makeEvalReady no student error!');
		foreach ($list['list'] as $item) {
			$temp = $this->listCourseEvalStat($tstat,$item['stid']);
			if ($temp['stat']===true&&count($temp['list'])>0) continue;
			$this->createCourseEvalStat($tstat,$item['stid']);
		}
	}
	// control/status table for staff
	function checkCourseEvalStat($table) { // CODE_SSEM_EVALSTAT
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('checkCourseEvalStat not-admin/mod error!');
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"stid","type"=>"INTEGER UNIQUE NOT NULL"),
				array("name"=>"stat","type"=>"INTEGER NOT NULL"));
			$tmore = array();
			array_push($tmore,
				"FOREIGN KEY(stid) REFERENCES ".$this->_usrtab."(id)");
			$this->table_create($table,$tdata,$tmore);
		}
	}
	function listCourseEvalStat($table,$stid=null) {
		if (!$this->table_exists($table))
			$this->throw_debug("listCourseEvalStat $table error!");
		$prep = "SELECT id,stat FROM $table";
		if ($stid!==null) {
			$prep = $prep." WHERE stid=:stid";
			$stid = intval($stid);
		}
		$stmt = $this->prepare($prep);
		if ($stid!==null) {
			if (!$stmt->bindValue(':stid',$stid,PDO::PARAM_INT))
				$this->throw_debug('listCourseEvalStat bind error!');
		}
		if (!$stmt->execute())
			$this->throw_debug('listCourseEvalStat execute error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!==false) {
			$pack['stat'] = true;
			foreach ($list as &$item) {
				$item['id'] = intval($item['id']);
				$item['stat'] = intval($item['stat']);
			}
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function createCourseEvalStat($table,$stid,$stat=EVAL_INIT) {
		if ($this->_dotype!==USER_ADMIN&&$this->_dotype!==USER_STAFF)
			$this->throw_debug('createCourseEvalStat not-admin/mod error!');
		if (!$this->table_exists($table))
			$this->throw_debug("createCourseEvalStat $table error!");
		if (empty($stid)||empty($stat))
			$this->throw_debug('createCourseEvalStat not enough info!');
		$stid = intval($stid);
		$stat = intval($stat);
		$find = $this->listCourseEvalStat($table,$stid);
		if ($find['stat']===true&&count($find['list'])>0)
			$this->throw_debug('createCourseEvalStat exists error!');
		$prep = "INSERT INTO $table (stid,stat) VALUES (:stid,:stat)";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		$stmt->bindValue(':stat',$stat,PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_debug('createCourseStudent exec error!');
		$stmt->closeCursor();
	}
	function finishCourseEvalStat($table,$stid,$stat=EVAL_DONE) {
		if ($this->_userid!==$stid)
			$this->throw_debug("finishCourseEvalStat not-own error!");
		if (!$this->table_exists($table))
			$this->throw_debug("finishCourseEvalStat $table error!");
		if (empty($stid)||empty($stat))
			$this->throw_debug('finishCourseEvalStat not enough info!');
		$stid = intval($stid);
		$stat = intval($stat);
		$find = $this->listCourseEvalStat($table,$stid);
		if ($find['stat']!==true)
			$this->throw_debug('finishCourseEvalStat not-exist error!');
		$prep = "UPDATE $table set stat=:stat WHERE stid=:stid";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':stat',$stat,PDO::PARAM_INT);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_debug('finishCourseEvalStat exec error!');
		$stmt->closeCursor();
	}
	// table for student to fill
	function checkCourseEval($table) { // CODE_SSEM_EVAL
		if (!$this->table_exists($table)) {
			$tdata = array();
			array_push($tdata,
				array("name"=>"id","type"=>"INTEGER PRIMARY KEY"),
				array("name"=>"stid","type"=>"INTEGER NOT NULL"),
				array("name"=>"ldid","type"=>"INTEGER NOT NULL"),
				array("name"=>"paq1","type"=>"INTEGER"),
				array("name"=>"paq2","type"=>"INTEGER"),
				array("name"=>"paq3","type"=>"INTEGER"),
				array("name"=>"paq4","type"=>"INTEGER"),
				array("name"=>"paq5","type"=>"INTEGER"),
				array("name"=>"paq6","type"=>"INTEGER"),
				array("name"=>"paq7","type"=>"INTEGER"),
				array("name"=>"paq8","type"=>"INTEGER"),
				array("name"=>"paq9","type"=>"INTEGER"),
				array("name"=>"pbq1","type"=>"INTEGER"),
				array("name"=>"pbq2","type"=>"INTEGER"),
				array("name"=>"pbq3","type"=>"INTEGER"),
				array("name"=>"pbq4","type"=>"INTEGER"),
				array("name"=>"pbq5","type"=>"INTEGER"),
				array("name"=>"pbq6","type"=>"INTEGER"),
				array("name"=>"pbq7","type"=>"INTEGER"),
				array("name"=>"pcq1","type"=>"INTEGER"),
				array("name"=>"pcq2","type"=>"INTEGER"),
				array("name"=>"pcq3","type"=>"INTEGER"),
				array("name"=>"pcq4","type"=>"INTEGER"),
				array("name"=>"pcq5","type"=>"INTEGER"),
				array("name"=>"pcq6","type"=>"INTEGER"),
				array("name"=>"pcq7","type"=>"INTEGER"),
				array("name"=>"pcq8","type"=>"INTEGER"),
				array("name"=>"mark","type"=>"TEXT NOT NULL"),
				array("name"=>"comm","type"=>"TEXT"));
			$tmore = array();
			array_push($tmore,
				"FOREIGN KEY(stid) REFERENCES users(id)",
				"FOREIGN KEY(ldid) REFERENCES users(id)");
			$this->table_create($table,$tdata,$tmore);
		}
	}
	function listCourseEval($table) {
		if (!$this->table_exists($table))
			$this->throw_debug("listCourseEval $table error!");
		$prep = "SELECT * FROM $table";
		$stmt = $this->prepare($prep);
		if (!$stmt->execute())
			$this->throw_debug('listCourseEval exec error!');
		$pack = [];
		$list = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($list!==false) {
			$pack['stat'] = true;
			$pack['list'] = $list;
		}
		else $pack['stat'] = false;
		return $pack;
	}
	function createCourseEval($table,$mark,$stid,$ldid,
			$paqx,$pbqx,$pcqx,$comm) {
		if (!$this->table_exists($table))
			$this->throw_debug("createCourseEval $table error!");
		$tstat = $table.'STAT';
		$stid = intval($stid);
		$ldid = intval($ldid);
		$find = $this->listCourseEvalStat($tstat,$this->_userid);
		if ($find['stat']!==true)
			$this->throw_debug('createCourseEvalStat no stat error!');
		$find = $find['list'][0]; // should be one-and-only item!
		if ($find['stat']===EVAL_DONE)
			$this->throw_debug('createCourseEvalStat done error!');
		$prep = "INSERT INTO ".$table;
		$prep = $prep." (stid,ldid";
		for ($loop=1;$loop<=9;$loop++)
			$prep = $prep.",paq".$loop;
		for ($loop=1;$loop<=7;$loop++)
			$prep = $prep.",pbq".$loop;
		for ($loop=1;$loop<=8;$loop++)
			$prep = $prep.",pcq".$loop;
		$prep = $prep.",mark,comm)";
		$prep = $prep." VALUES (:stid,:ldid";
		for ($loop=1;$loop<=9;$loop++)
			$prep = $prep.",:paq".$loop;
		for ($loop=1;$loop<=7;$loop++)
			$prep = $prep.",:pbq".$loop;
		for ($loop=1;$loop<=8;$loop++)
			$prep = $prep.",:pcq".$loop;
		$prep = $prep.",:mark,:comm)";
		$stmt = $this->prepare($prep);
		$stmt->bindValue(':stid',$stid,PDO::PARAM_INT);
		$stmt->bindValue(':ldid',$ldid,PDO::PARAM_INT);
		$stmt->bindValue(':mark',$mark,PDO::PARAM_STR);
		$stmt->bindValue(':comm',$comm,PDO::PARAM_STR);
		for ($loop=1;$loop<=9;$loop++)
			$stmt->bindValue(':paq'.$loop,$paqx['A'.$loop],PDO::PARAM_INT);
		for ($loop=1;$loop<=7;$loop++)
			$stmt->bindValue(':pbq'.$loop,$pbqx['B'.$loop],PDO::PARAM_INT);
		for ($loop=1;$loop<=8;$loop++)
			$stmt->bindValue(':pcq'.$loop,$pcqx['C'.$loop],PDO::PARAM_INT);
		if (!$stmt->execute())
			$this->throw_debug('createCourseEval exec error!');
		$stmt->closeCursor();
		// mark done
		$this->finishCourseEvalStat($tstat,$this->_userid);
	}
}
?>
