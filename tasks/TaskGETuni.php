<?php
require_once dirname(__FILE__).'/TaskGET.php';
class TaskGETuni extends TaskGET {
	function checkParam() {
		$test = parent::checkParam();
		if (isset($test['get'])) {
			if ($test['get']!=='do')
				$this->throw_debug('Invalid \''.$test['get'].'\'!');
			// start checking
			if ($_GET['do']=='viewstaff') {
				// page to view/download list of staffs
				$page = $this->PrepPage('PageStaff');
				if (isset($_GET['fmt'])&&$_GET['fmt']=='csv')
					$page->sendCSV();
				else $page->Show();
			}
			else if ($_GET['do']=='viewcourse') {
				// page to view/download list of courses
				$page = $this->PrepPage('PageCourse');
				if (isset($_GET['fmt'])&&$_GET['fmt']=='csv')
					$page->sendCSV();
				else $page->Show();
			}
			else if ($_GET['do']=='rstpass') $this->ShowPage('PagePassRst');
			else if ($_GET['do']=='editcourse')
				$this->ShowPage('PageCourseEdit');
			else if ($_GET['do']=='ccompcsv') {
				$page = $this->PrepPage('PageSemCourse');
				$page->sendCSV(TEMPLATE_COURSEC);
			}
			else if ($_GET['do']=='impcourse') {
				if (!isset($_GET['ssem'])||!isset($_GET['code']))
					$this->throw_debug('Invalid course implementation!');
				$this->ShowPage('PageSemCourse');
			}
			else if ($_GET['do']=='cstudcsv') {
				$page = $this->PrepPage('PageSemCourse');
				$page->sendCSV(TEMPLATE_STUDENT);
			}
			else if ($_GET['do']=='stmarkcsv') {
				$page = $this->PrepPage('PageSemCourse');
				$page->sendCSV(TEMPLATE_STMARKS);
			}
			else if ($_GET['do']=='editccmp')
				$this->ShowPage('PageSemCourseEdit');
			else if ($_GET['do']=='viewsemcourse')
				$this->ShowPage('PageCourseImp');
			else if ($_GET['do']=='viewstudent') $this->ShowPage('PageStudent');
			else if ($_GET['do']=='selectstaff')
				$this->ShowPage('PageStaffSelect');
			else if ($_GET['do']=='removeimp')
				$this->ShowPage('PageCourseImpCh');
			else if ($_GET['do']=='removesemstaff')
				$this->ShowPage('PageStaffSemRemove');
			else if ($_GET['do']=='selectsemstaff')
				$this->ShowPage('PageStaffSem');
			else if ($_GET['do']=='studeval')
				$this->ShowPage('PageStudentEval');
			else if ($_GET['do']=='makeeval')
				$this->ShowPage('PageStudentEvalPrep');
			else if ($_GET['do']=='vieweval')
				$this->ShowPage('PageStudentEvalView');
			else if ($_GET['do']=='deactiv8')
				$this->ShowPage('PageStudentActiveCh');
			else if ($_GET['do']=='reactiv8')
				$this->ShowPage('PageStudentActiveCh');
			else if ($_GET['do']=='disableStud')
				$this->ShowPage('PageStudentEnableCh');
			else if ($_GET['do']=='enableStud')
				$this->ShowPage('PageStudentEnableCh');
			else {
				// let child classes try?
				$test['get'] = 'do';
			}
		}
		return $test;
	}
}
?>
