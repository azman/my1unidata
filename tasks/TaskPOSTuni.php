<?php
require_once dirname(__FILE__).'/TaskPOST.php';
class TaskPOSTuni extends TaskPOST {
	function checkPass() {
		parent::checkPass();
		if (array_key_exists('postChCComp',$_POST)) {
			// change course component
			$page = "PageSemCourseCh";
			if (!array_key_exists('cCCID',$_POST)||
					!array_key_exists('cName',$_POST)||
					!array_key_exists('cText',$_POST)||
					!array_key_exists('cRawM',$_POST)||
					!array_key_exists('cPctM',$_POST)) {
				$this->throw_debug("Missing params for $page!");
			}
			$this->ShowPage($page);
		}
		if (array_key_exists('postChStudentEval',$_POST)) {
			// post student evaluation
			$page = "PageStudentEvalCh";
			if (!array_key_exists('code',$_POST)||
					!array_key_exists('unid',$_POST)||
					!array_key_exists('ssem',$_POST)||
					!array_key_exists('coid',$_POST)||
					!array_key_exists('stid',$_POST)) {
				$this->throw_debug("Missing params for $page!");
			}
			$this->ShowPage($page);
		}
		if (array_key_exists('unid',$_POST)) {
			// data upload
			if (array_key_exists('aCmd',$_POST)) {
				$page = $this->PrepPage('PageLoad'); //ADMIN_DATA_CLASS???
				$page->Show();
			}
		}
		else if (array_key_exists('cCode',$_POST)&&
				array_key_exists('cName',$_POST)&&
				array_key_exists('cUnit',$_POST)&&
				array_key_exists('cCoId',$_POST)) {
			// change course
			$this->ShowPage('PageCourseCh');
		}
		else if (array_key_exists('cCoId',$_POST)&&
				array_key_exists('cCode',$_POST)&&
				array_key_exists('cType',$_POST)&&
				array_key_exists('postSelectStaff',$_POST)) {
			// change course-staff assignment
			$this->ShowPage('PageStaffSelectCh');
		}
		else if (array_key_exists('code',$_POST)&&
				array_key_exists('ssem',$_POST)&&
				array_key_exists('postSelectStaffSem',$_POST)) {
			// change courses-staffs assignment
			$this->ShowPage('PageStaffSemCh');
		}
		$this->throw_debug('[DEBUG]'.json_encode($_POST));
	}
}
?>
