<?php
require_once dirname(__FILE__).'/../include/config.php';
require_once dirname(__FILE__).'/../include/Thing.php';
require_once dirname(__FILE__).'/../pages/Page.php';
class Theme extends Thing {
	protected $_page;
	function __construct($page=null) {
		if (!is_a($page,'Page'))
			$this->throw_this("Invalid Page for Theme!");
		$this->_page = $page;
	}
	function menu_item_type($link,$text,$type=MENUITEM_LINK) {
		return [ "type"=>$type, "that"=>$link, "text"=>$text ];
	}
	function menu_list_item_link($list,$link,$text) {
		if ($list===null) $list = [];
		array_push($list,$this->menu_item_type($link,$text));
		return $list;
	}
	function menu_list_item_linkback($list,
			$back=DEFAULT_BACK_COUNT,$text='Back') {
		if ($list===null) $list = [];
		$link = $this->_page->text_link_back($back);
		array_push($list,$this->menu_item_type($link,$text));
		return $list;
	}
	function menu_list_item_command($list,$link,$text) {
		if ($list===null) $list = [];
		array_push($list,$this->menu_item_type($link,$text,MENUITEM_CMD_));
		return $list;
	}
}
?>
